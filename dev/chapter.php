<?php

    require "config.php";

    $result = array();
    
    $lihat = mysqli_query($con, "SELECT * FROM chapter");
    while ($a = mysqli_fetch_array($lihat)) {
        # code...
        $nilai['id'] = $a['id'];
        $nilai['title'] = $a['title'];
        $nilai['moment'] = $a['moment'];
        $nilai['cover'] = $a['cover'];
        $nilai['color'] = $a['color'];
        $nilai['file'] = $a['file'];
        
        array_push($result,$nilai);
    }
    echo json_encode($result);
?>