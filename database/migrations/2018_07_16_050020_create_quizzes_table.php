<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_relasi')->unsigned();
            $table->foreign('id_relasi')->references('id')->on('guru_matpels')->onDelete('CASCADE');
            $table->integer('id_pembuat')->unsigned();
            $table->foreign('id_pembuat')->references('id')->on('users')->onDelete('CASCADE');
            $table->string('judul');
            $table->text('info');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quizzes');
    }
}
