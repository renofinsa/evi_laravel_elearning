<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManajemenkelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manajemenkelas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_walikelas')->unsigned();
            $table->foreign('id_walikelas')->references('id')->on('users')->onDelete('CASCADE');
            $table->integer('id_ketuakelas')->unsigned();
            $table->foreign('id_ketuakelas')->references('id')->on('users')->onDelete('CASCADE');
            $table->integer('id_kelas')->unsigned();
            $table->foreign('id_kelas')->references('id')->on('kelas')->onDelete('CASCADE');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manajemenkelas');
    }
}
