<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuruMatpelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guru_matpels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_guru')->unsigned();
            $table->foreign('id_guru')->references('id')->on('users')->onDelete('CASCADE');
            $table->integer('id_matpel')->unsigned();
            $table->foreign('id_matpel')->references('id')->on('matapelajarans')->onDelete('CASCADE');
            $table->integer('id_kelas')->unsigned();
            $table->foreign('id_kelas')->references('id')->on('kelas')->onDelete('CASCADE');
            $table->integer('id_semester')->unsigned();
            $table->foreign('id_semester')->references('id')->on('semester')->onDelete('CASCADE');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guru_matpels');
    }
}
