<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('email',123)->unique();
            $table->string('password');
            $table->integer('level');
            $table->string('tempatlahir');
            $table->date('tanggallahir');
            $table->integer('jk');
            $table->text('alamat');
            $table->string('telepon');
            $table->text('foto');
            $table->integer('id_agama')->unsigned();
            $table->foreign('id_agama')->references('id')->on('agamas')->onDelete('CASCADE');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
