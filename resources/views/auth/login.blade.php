@extends('layouts.app1')

@section('content')
<div class="login-box">

  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Welcome SMAN 25 Jakarta</p>
      <div class="login-logo">
        <img src="../dist/img/logo.png"  alt="User Image">
      </div>

                  <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                    @csrf
                    <div class="form-group has-feedback">
                      <span class="fa fa-envelope form-control-feedback"></span>
                      <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                      @if ($errors->has('email'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                    </div>
                    <div class="form-group has-feedback">
                      <span class="fa fa-lock form-control-feedback"></span>
                      <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                      @if ($errors->has('password'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                    </div>
                    <div class="row">
                      {{-- <div class="col-8">
                        <div class="checkbox icheck p-4">
                          <label>
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                          </label>
                        </div>
                      </div> --}}
                      <!-- /.col -->
                      <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                      </div>
                      <!-- /.col -->
                    </div>
                  </form>
                  {{-- <p class="mb-1">
                    <a href="{{ route('password.request') }}">I forgot my password</a>
                  </p>
                  <p class="mb-0">
                    <a href="{{ route('register') }}" class="text-center">Register a new membership</a>
                  </p> --}}
        </div>
    </div>
</div>
@endsection
