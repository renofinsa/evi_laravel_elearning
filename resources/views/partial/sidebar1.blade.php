<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-image: url('../../dist/img/bg_baru.jpg'">
  <!-- Brand Logo -->
  <a href="#" class="brand-link">
    @if(checkPermission(['admin']))
      <img style="width : 50px" src="../../storage/app/public/upload_foto/{{auth::user()->foto}}" class="img-circle elevation-2" alt="User Image">
    @endif
    @if(checkPermission(['siswa']))
      <img style="width : 50px" src="../../storage/app/public/upload_foto_siswa/{{auth::user()->foto}}" class="img-circle elevation-2" alt="User Image">
    @endif
    @if(checkPermission(['pengajar']))
      <img src="../../storage/app/public/upload_foto/{{auth::user()->foto}}" class="img-circle elevation-2" alt="User Image">
    @endif
    <span style="width : 50px" class="brand-text font-weight-light" style="color : #fff">
        {{ Auth::user()->nama }}
    </span>
  </a>


  <!-- Sidebar -->



    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
      @if(checkPermission(['pengajar']))
        <li class="nav-header"></li>
        <li class="nav-item has-treeview menu-open">
          <a href="/home" class="nav-link active">
            <i class="nav-icon fa fa-dashboard"></i>
            <p>
              Home
            </p>
          </a>
        </li>
        {{-- <li class="nav-item has-treeview">
          <a href="/matapelajaran" class="nav-link">
            <i class="nav-icon fa fa-tree"></i>
            <p>
              Mata Pelajaran
            </p>
          </a>
        </li> --}}
        <li class="nav-item has-treeview">
          <a href="/quiz" class="nav-link">
            <i class="nav-icon fa fa-edit"></i>
            <p>
              Manajemen Tugas/Quiz
            </p>
          </a>
        </li>
        @endif
        @if(checkPermission(['siswa']))
        <li class="nav-header"></li>
        <li class="nav-item has-treeview menu-open">
          <a href="/home" class="nav-link active">
            <i class="nav-icon fa fa-dashboard"></i>
            <p>
              Home
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="/quizsiswa" class="nav-link">
            <i class="nav-icon fa fa-pie-chart"></i>
            <p>
              Tugas/Quiz
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="/materi" class="nav-link">
            <i class="nav-icon fa fa-tree"></i>
            <p>
              Materi
            </p>
          </a>
        </li>
        @endif
        @if(checkPermission(['admin']))
        <li class="nav-header"></li>
        <li class="nav-item has-treeview menu-open">
          <a href="/home" class="nav-link active">
            <i class="nav-icon fa fa-dashboard"></i>
            <p>
              Home
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="pages/widgets.htm" class="nav-link">
            <i class="nav-icon fa fa-th"></i>
            <p>
              Manajemen Akun
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/pengajar" class="nav-link">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Manajemen Pengajar</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/siswa" class="nav-link">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Manajemen Siswa</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview">
          <a href="/kelas" class="nav-link">
            <i class="nav-icon fa fa-pie-chart"></i>
            <p>
              Manajemen Kelas
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="/semester" class="nav-link">
            <i class="nav-icon fa fa-tree"></i>
            <p>
              Manajemen Semester
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="/matapelajaran" class="nav-link">
            <i class="nav-icon fa fa-tree"></i>
            <p>
              Mata Pelajaran
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="/matpelpengajar" class="nav-link">
            <i class="nav-icon fa fa-pie-chart"></i>
            <p>
              Mata Pelajaran & Pengajar
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="/quiz" class="nav-link">
            <i class="nav-icon fa fa-edit"></i>
            <p>
              Manajemen Tugas/Quiz
            </p>
          </a>
        </li>

        @endif
        @if(checkPermission(['admin', 'pengajar']))
          <li class="nav-item has-treeview">
            <a href="/materi" class="nav-link">
              <i class="nav-icon fa fa-table"></i>
              <p>
                Materi
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="/berita" class="nav-link">
              <i class="nav-icon fa fa-table"></i>
              <p>
                Berita
              </p>
            </a>
          </li>
        @endif
      </ul>
    </nav>
    <!-- /.sidebar-menu -->

  <!-- /.sidebar -->
</aside>
