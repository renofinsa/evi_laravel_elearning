@extends('master')
@section('title', 'Manajemen Siswa')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      @if(\Session::has('alert'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{\Session::get('alert')}}</p>
      </div>
      <br />
      @endif
      @if(\Session::has('failed'))
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <p>{{\Session::get('failed')}}</p>
      </div>
      <br />
      @endif
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Manajemen Siswa</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Index</h3>

            <div class="card-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                  <button class="btn btn-success btn-sm" type="button" name="button" data-toggle="modal" data-target="#tambah">Tambah Siswa</button>
              </div>
              <!-- MODAL TAMBAH -->
              <div id="tambah" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Tambah Siswa</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('siswa.store')}}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group has-feedback">
                          <label for="">Foto (Jpg,Png | Max 2000kb)</label>
                          <input type="file" name="foto" value="lorem" class="form-control{{ $errors->has('foto') ? ' is-invalid' : '' }}"
                          placeholder="Joko Anwar">
                          @if ($errors->has('foto'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('foto') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nama Lengkap</label>
                          <input style="text-transform : capitalize" type="text" name="nama" value="" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" required
                          placeholder="Joko Anwar">
                          @if ($errors->has('nama'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('nama') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">NISN (Nomer Induk Siswa Nasional)</label>
                          <input type="number" name="nis" value="" class="form-control{{ $errors->has('nis') ? ' is-invalid' : '' }}" required
                          placeholder="102030405060">
                          @if ($errors->has('nis'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('nis') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="kelas">Pilih Kelas</label>
                          <select class="form-control{{ $errors->has('id_kelas') ? ' is-invalid' : '' }}" name="id_kelas" id="" required>
                            <option value="0">Pilih Kelas</option>
                            @foreach ($kelas as $kelass)
                              <option value="{{ $kelass->id}}">{{ $kelass->kelas}}</option>
                            @endforeach
                          </select>

                                @if ($errors->has('id_kelas'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('id_kelas') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Email</label>
                          <input type="email" name="email" value="" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required
                          placeholder="email@domain.com">
                          @if ($errors->has('email'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Kata Sandi</label>
                          <input type="text" name="kode" value="" class="form-control" required placeholder="masukan kata sandi kamu disini">
                          @if ($errors->has('kode'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('kode') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="agama">Agama</label>
                          <select class="form-control" name="id_agama" id="" required>
                            @foreach ($agama as $agamas)
                              <option value="{{ $agamas->id}}">{{ $agamas->agama}}</option>
                            @endforeach
                          </select>

                                @if ($errors->has('id_agama'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('id_agama') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Tempat Lahir</label>
                          <input id="tempatlahir" type="text" class="form-control{{ $errors->has('tempatlahir') ? ' is-invalid' : '' }}" name="tempatlahir" required placeholder="Cth : Jakarta">
                          @if ($errors->has('tempatlahir'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('tempatlahir') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Tanggal Lahir</label>
                          <input id="tanggallahir" type="date" class="form-control{{ $errors->has('tanggallahir') ? ' is-invalid' : '' }}" name="tanggallahir" required >
                          @if ($errors->has('tanggallahir'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('tanggallahir') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Jenis Kelamin</label>
                          <div class="row">
                            <div class="col-md-6">
                              <input type="radio" name="jk" value="1" checked class="{{ $errors->has('gender') ? ' is-invalid' : '' }}"> Pria<br>
                            </div>
                            <div class="col-md-6">
                                <input type="radio" name="jk" value="2" class="{{ $errors->has('gender') ? ' is-invalid' : '' }}"> Wanita<br>
                            </div>
                          </div>
                          @if ($errors->has('jk'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('jk') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nomer Telepon</label>
                          <input type="number" name="nomertelepon"  class="form-control{{ $errors->has('nomertelepon') ? ' is-invalid' : '' }}" required placeholder="+62822XXXXX">
                          @if ($errors->has('nomertelepon'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('nomertelepon') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Alamat</label>
                          <textarea name="alamat" class="form-control{{ $errors->has('alamat') ? ' is-invalid' : '' }}" rows="5" cols="80" placeholder="masukan alamat kamu disini" required></textarea>
                          @if ($errors->has('alamat'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('alamat') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nama Ayah</label>
                          <input type="text" name="namaayah" value="" class="form-control{{ $errors->has('namaayah') ? ' is-invalid' : '' }}" required
                          placeholder="Jodi Hermawan">
                          @if ($errors->has('namaayah'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('namaayah') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nama Ibu</label>
                          <input type="text" name="namaibu" value="" class="form-control{{ $errors->has('namaibu') ? ' is-invalid' : '' }}" required
                          placeholder="Nur Hasanah">
                          @if ($errors->has('namaibu'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('namaibu') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <button type="submit" name="button" class="btn btn-info">Simpan</button>
                          <button type="button" name="button" class="btn btn-danger" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                    <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                  </div>

                </div>
              </div>
              <!-- MODAL TAMBAH -->
            </div>
          </div>
          <!-- /.card-header -->
          <div class="clearfix">

          </div>
          <div class="card-body table-responsive p-3">
            <table id="example1" class="table table-hover table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>NIS</th>
                <th>Nama Lengkap</th>
                <th>Jenis Kelamin</th>
                <th>Kelas</th>
                <th>Foto Profil</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 0; ?>
              @foreach ($siswa as $sis)
              <?php $no++ ?>

              <tr>
                <td>{{ $no }}</td>
                <td>{{ $sis->nis}}</td>
                <td>{{ $sis->user->nama}}</td>
                @if($sis->user->jk =='1')

                    <td>Pria</td>
                  @else
                    <td>Wanita</td>

                @endif
                <td>{{ $sis->kelas->kelas}}</td>
                <td><img src="storage/app/public/upload_foto_siswa/{{$sis->user->foto}}" width="100px"></td>

                <td>
                  <button class="btn btn-primary btn-sm" type="button" name="button" data-toggle="modal" data-target="#edit_{{$sis->id}}">Ubah</button>
                  <button class="btn btn-danger btn-sm" type="button" name="button" data-toggle="modal" data-target="#delete_{{$sis->id}}">Hapus</button>
                  <button class="btn btn-success btn-sm" type="button" name="button" data-toggle="modal" data-target="#lihat_{{$sis->id}}">Detail</button>
                </td>
              </tr>
              <!-- MODAL LIHAT -->
              <div id="lihat_{{$sis->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">NIS : {{ $sis->nis}}</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <img src="storage/app/public/upload_foto_siswa/{{$sis->user->foto}}" width="150px">
                          </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                              <h5>Nama : {{ $sis->user->nama}}</h5>
                            </div>
                            <div class="form-group">
                              <h5>Jenis Kelamin :
                                @if($sis->user->jk =='1')

                                    Pria
                                  @else
                                    Wanita

                                @endif

                              </h5>
                            </div>
                            <div class="form-group">
                              <h5>Kelas : {{ $sis->kelas->kelas}}</h5>
                            </div>
                            <div class="form-group">
                              <h5>Agama : {{ $sis->user->agama->agama}}</h5>
                            </div>
                        </div>
                      </div>
                      <hr>
                      <h5><b>Tempat, Tanggal Lahir : </b><br>{{ $sis->user->tempatlahir }}, {{ $sis->user->tanggallahir}}</h5>
                      <h5><b>Nomer Telepon : </b><br>{{ $sis->user->telepon }}</h5>
                      <h5><b>Email : </b><br>{{ $sis->user->email }}</h5>
                      <h5><b>Nama Ayah : </b><br>{{ $sis->nama_ayah }} </h5>
                      <h5><b>Nama Ibu : </b><br>{{ $sis->nama_ibu }} </h5>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL LIHAT -->
              <!-- MODAL EDIT -->
              <div id="edit_{{$sis->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Ubah Data Siswa</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('siswa.update', $sis->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group">
                          <center><img src="storage/app/public/upload_foto_siswa/{{$sis->user->foto}}" width="100px"></center>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Foto (Jpg,Png | Max 2000kb)</label>
                          <input type="hidden" value="{{$sis->user->foto}}" name="foto_lama">
                          <input type="file" name="foto" class="form-control{{ $errors->has('foto') ? ' is-invalid' : '' }}">
                          @if ($errors->has('foto'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('foto') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nama Lengkap</label>
                          <input style="text-transform : capitalize" type="text" name="nama" value="{{ $sis->user->nama}}" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" required
                          placeholder="Joko Anwar">
                          @if ($errors->has('nama'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('nama') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">NISN (Nomer Induk Siswa Nasional)</label>
                          <input type="number" name="nis" value="{{ $sis->nis}}" class="form-control{{ $errors->has('nis') ? ' is-invalid' : '' }}" required
                          placeholder="102030405060">
                          @if ($errors->has('nis'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('nis') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="kelas">Pilih Kelas</label>
                          <select class="form-control{{ $errors->has('id_kelas') ? ' is-invalid' : '' }}" name="id_kelas" id="" required>
                            <option value="{{ $sis->id_kelas}}">{{ $sis->kelas->kelas}}</option>
                            @foreach ($kelas as $kelass)
                              <option value="{{ $kelass->id}}">{{ $kelass->kelas}}</option>
                            @endforeach
                          </select>

                                @if ($errors->has('id_kelas'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('id_kelas') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Email</label>
                          <input type="hidden" name="email_lama" value="{{$sis->user->email}}">
                          <input type="email" name="email" value="{{ $sis->user->email}}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required
                          placeholder="email@domain.com">
                          @if ($errors->has('email'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Kata Sandi</label>
                          <input type="password" name="password" value="{{ $sis->user->password}}" class="form-control"  required placeholder="">
                          <input type="hidden" name="password_lama" value="{{ $sis->user->password}}" class="form-control" required placeholder="masukan kata sandi kamu disini">

                        </div>
                        <div class="form-group has-feedback">
                          <label for="agama">Agama</label>
                          <select class="form-control" name="id_agama" id="" required>
                            <option  value="{{ $sis->user->id_agama}}">{{ $sis->user->agama->agama}}</option>
                            @foreach ($agama as $agamas)
                              <option value="{{ $agamas->id}}">{{ $agamas->agama}}</option>
                            @endforeach
                          </select>

                                @if ($errors->has('id_agama'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('id_agama') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Tempat Lahir</label>
                          <input value="{{ $sis->user->tempatlahir}}" id="tempatlahir" type="text" class="form-control{{ $errors->has('tempatlahir') ? ' is-invalid' : '' }}" name="tempatlahir" required placeholder="Cth : Jakarta">
                          @if ($errors->has('tempatlahir'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('tempatlahir') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Tanggal Lahir</label>
                          <input value="{{ $sis->user->tanggallahir}}" id="tanggallahir" type="date" class="form-control{{ $errors->has('tanggallahir') ? ' is-invalid' : '' }}" name="tanggallahir" required >
                          @if ($errors->has('tanggallahir'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('tanggallahir') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Jenis Kelamin</label>
                          <div class="row">
                            <div class="col-md-6">
                              <input type="radio" name="jk" value="1"
                              @if ($sis->user->jk =='1' )
                                checked
                              @endif
                                class="{{ $errors->has('jk') ? ' is-invalid' : '' }}"> Pria<br>

                            </div>
                            <div class="col-md-6">
                                <input type="radio" name="jk" value="2"
                                 @if ($sis->user->jk =='2' )
                                  checked
                                @endif
                                class="{{ $errors->has('jk') ? ' is-invalid' : '' }}"> Wanita<br>

                            </div>
                          </div>
                          @if ($errors->has('jk'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('jk') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nomer Telepon</label>
                          <input value="{{ $sis->user->telepon}}" type="number" name="nomertelepon"  class="form-control{{ $errors->has('nomertelepon') ? ' is-invalid' : '' }}" required placeholder="+62822XXXXX">
                          @if ($errors->has('nomertelepon'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('nomertelepon') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Alamat</label>
                          <textarea  name="alamat" class="form-control{{ $errors->has('alamat') ? ' is-invalid' : '' }}" rows="5" cols="80" placeholder="masukan alamat kamu disini" required>{{ $sis->user->alamat}}</textarea>
                          @if ($errors->has('alamat'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('alamat') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nama Ayah</label>
                          <input value="{{ $sis->nama_ayah}}" type="text" name="namaayah" value="" class="form-control{{ $errors->has('namaayah') ? ' is-invalid' : '' }}" required
                          placeholder="Jodi Hermawan">
                          @if ($errors->has('namaayah'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('namaayah') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nama Ibu</label>
                          <input value="{{ $sis->nama_ibu}}" type="text" name="namaibu" value="" class="form-control{{ $errors->has('namaibu') ? ' is-invalid' : '' }}" required
                          placeholder="Nur Hasanah">
                          @if ($errors->has('namaibu'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('namaibu') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <button type="submit" name="button" class="btn btn-info">Simpan</button>
                          <button type="button" name="button" class="btn btn-danger" data-dismiss=modal>Batal</button>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL EDIT -->
              <!-- MODAL DELETE -->
              <div id="delete_{{$sis->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Hapus Data</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('siswa.destroy', $sis->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <p>Yakin Anda ingin menghapus data {{$sis->user->nama}}</p>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-danger btn-sm">Hapus</button>
                          <button type="button" name="button" class="btn btn-secondary btn-sm" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL DELETE -->

              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>NIS</th>
                <th>Nama Lengkap</th>
                <th>Jenis Kelamin</th>
                <th>Kelas</th>
                <th>Foto Profil</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
            </table>
          </div>
          <!-- /.card-body -->

        </div>
        <!-- /.card -->
      </div>
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
@endsection
