@extends('master')
@section('title', 'Manajemen Kelas')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      @if(\Session::has('alert'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{\Session::get('alert')}}</p>
      </div>
      <br />
      @endif
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Manajemen Kelas</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Index</h3>

            <div class="card-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                  <button class="btn btn-success btn-sm" type="button" name="button" data-toggle="modal" data-target="#tambah">Tambah Kelas</button>
              </div>
              <!-- MODAL TAMBAH -->
              <div id="tambah" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Tambah Kelas</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('kelas.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                          <label for="">Kelas</label>
                          <input type="text" name="kelas" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="">Ruangan</label>
                          <input type="text" name="ruangan" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-info">Simpan</button>
                          <button type="button" name="button" class="btn btn-danger" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                    <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                  </div>

                </div>
              </div>
              <!-- MODAL TAMBAH -->
            </div>
          </div>
          <!-- /.card-header -->
          <div class="clearfix">

          </div>
          <div class="card-body table-responsive p-3">
            <table id="example1" class="table table-hover table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Kelas</th>
                <th>Ruangan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 0; ?>
              @foreach($kelas as $k)
              <?php $no++ ?>
              <tr>
                <td>{{$no}}</td>
                <td>{{$k->kelas}}</td>
                <td>{{$k->ruangan}}</td>
                <td>
                  <button class="btn btn-primary btn-sm" type="button" name="button" data-toggle="modal" data-target="#edit_{{$k->id}}">Ubah</button>
                  <button class="btn btn-danger btn-sm" type="button" name="button" data-toggle="modal" data-target="#delete_{{$k->id}}">Hapus</button>
                </td>
              </tr>

              <!-- MODAL EDIT -->
              <div id="edit_{{$k->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Ubah Kelas</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('kelas.update', $k->id)}}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group">
                          <label for="">Kelas</label>
                          <input type="text" name="kelas" value="{{$k->kelas}}" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="">Ruangan</label>
                          <input type="text" name="ruangan" value="{{$k->ruangan}}" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-success btn-sm">Simpan</button>
                          <button type="button" name="button" class="btn btn-warning btn-sm" data-dismiss=modal>Batal</button>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL EDIT -->
              <!-- MODAL DELETE -->
              <div id="delete_{{$k->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Hapus Kelas</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('kelas.destroy', $k->id)}}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <p>Yakin Anda ingin menghapus kelas {{$k->kelas}}</p>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-danger btn-sm">Hapus</button>
                          <button type="button" name="button" class="btn btn-secondary btn-sm" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL DELETE -->
            @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Kelas</th>
                <th>Ruangan</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
            </table>
          </div>
          <!-- /.card-body -->

        </div>
        <!-- /.card -->
      </div>
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
@endsection
