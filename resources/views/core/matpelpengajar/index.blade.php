@extends('master')
@section('title', 'Manajemen Mata Pelajaran')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      @if(\Session::has('alert'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{\Session::get('alert')}}</p>
      </div>
      <br />
      @endif
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Manajemen Mata Pelajaran & Pengajar</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!--- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Index</h3>

            <div class="card-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                  <button class="btn btn-success btn-sm" type="button" name="button" data-toggle="modal" data-target="#tambah">Tambah Data</button>
              </div>
              <!-- MODAL TAMBAH -->
              <div id="tambah" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Tambah Data</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('matpelpengajar.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                          <label for="">Semester</label>
                            <select class="form-control" name="idsemester">
                              <option value="0">Semester</option>
                              @foreach ($semester as $ss)
                                  <option value="{{ $ss->id}}">{{ $ss->semester }}</option>
                              @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                          <label for="">Mata Pelajaran</label>
                            <select class="form-control" name="idmatpel">
                              <option value="0">Pilih Mata Pelajaran</option>
                              @foreach ($matpel as $m)
                                  <option value="{{ $m->id}}">{{ $m->matpel }}</option>
                              @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                          <label for="">Pengajar</label>
                            <select class="form-control" name="idguru">
                              <option value="0">Pilih Pengajar</option>
                              @foreach ($guru as $u)
                                  <option value="{{ $u->users->id}}">{{ $u->users->nama }}</option>
                              @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                          <label for="">Kelas</label>
                            <select class="form-control" name="idkelas">
                              <option value="0">Pilih Kelas</option>
                              @foreach ($kelas as $k)
                                  <option value="{{ $k->id}}">{{ $k->kelas }}</option>
                              @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-info">Simpan</button>
                          <button type="button" name="button" class="btn btn-danger" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                    <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                  </div>

                </div>
              </div>
              <!-- MODAL TAMBAH -->
            </div>
          </div>
          <!-- /.card-header -->
          <div class="clearfix">

          </div>
          <div class="card-body table-responsive p-3">
            <table id="example1" class="table table-hover table-bordered table-striped">
              <thead>

              <tr>
                <th>No</th>
                <th>Kode Mata Pelajaran</th>
                <th>Guru</th>
                <th>Mata Pelajaran</th>
                <th>Kelas</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 0; ?>
              @foreach ($gurumatpel as $key)
                <?php $no++?>
                <tr>
                  <td style="background-color : $color">{{ $no}}</td>
                  <td>{{$key->matpel->kodematpel }} <span style="font-size : 12px; color : red">( {{$key->semester->semester }} )</span> </td>
                  <td>{{$key->users->nama }}</td>
                  <td>{{$key->matpel->matpel}}</td>
                  <td>{{$key->kelas->kelas}}</td>
                  <td>
                    <button class="btn btn-primary btn-sm" type="button" name="button" data-toggle="modal" data-target="#edit_{{$key->id}}">Ubah</button>
                    <button class="btn btn-danger btn-sm" type="button" name="button" data-toggle="modal" data-target="#delete_{{$key->id}}">Hapus</button>
                  </td>
                </tr>


              <!-- MODAL EDIT -->
              <div id="edit_{{$key->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Ubah Mata Pelajaran</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('matpelpengajar.update', $key->id)}}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group">
                          <label for="">Semester</label>
                            <select class="form-control" name="idsemester">
                              <option value="{{ $key->id_semester}}">{{ $key->semester->semester }}</option>
                              @foreach ($semester as $ss)
                                  <option value="{{ $ss->id}}">{{ $ss->semester }}</option>
                              @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                          <label for="">Mata Pelajaran</label>
                            <select class="form-control" name="idmatpel">
                              <option value="{{ $key->id_matpel}}">{{ $key->matpel->matpel }}</option>
                              @foreach ($matpel as $m)
                                  <option value="{{ $m->id}}">{{ $m->matpel }}</option>
                              @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                          <label for="">Pengajar</label>
                            <select class="form-control" name="idguru">
                              <option value="{{ $key->id_guru}}">{{ $key->users->nama }}</option>
                              @foreach ($guru as $u)
                                  <option value="{{ $u->users->id}}">{{ $u->users->nama }}</option>
                              @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                          <label for="">Kelas</label>
                            <select class="form-control" name="idkelas">
                              <option value="{{ $key->id_kelas}}">{{ $key->kelas->kelas }}</option>
                              @foreach ($kelas as $k)
                                  <option value="{{ $k->id}}">{{ $k->kelas }}</option>
                              @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                          <label for="">Status</label>
                            <div class="row">
                              <div class="col-md-6">
                                <input type="radio" name="status" value="1" @if ($key->status == 1) checked @endif>Aktif
                              </div>
                              <div class="col-md-6">
                                <input type="radio" name="status" value="0" @if ($key->status == 0) checked @endif>Tidak Aktif
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-success btn-sm">Simpan</button>
                          <button type="button" name="button" class="btn btn-warning btn-sm" data-dismiss=modal>Batal</button>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL EDIT -->
              <!-- MODAL DELETE -->
              <div id="delete_{{$key->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Hapus Mata Pelajaran</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('matpelpengajar.destroy', $key->id)}}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <p>Yakin Anda ingin menghapus Relasi Mata Pelajaran <b style="color: red"><u>{{ $key->matpel->matpel}}</b></u>
                            dengan guru pengajar <u><b style="color: red">{{ $key->users->nama}}</b></u> pada <b style="color: red"><u>{{ $key->semester->semester}}</b></u> ??
                         </p>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-danger btn-sm">Hapus</button>
                          <button type="button" name="button" class="btn btn-secondary btn-sm" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL DELETE -->
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Kode Mata Pelajaran</th>
                <th>Guru</th>
                <th>Mata Pelajaran</th>
                <th>Kelas</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
            </table>
          </div>
          <!-- /.card-body -->

        </div>
        <!-- /.card -->
      </div>
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
@endsection
