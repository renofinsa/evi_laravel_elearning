@extends('master')
@section('title', Auth::user()->nama)

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      @if(\Session::has('alert'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{\Session::get('alert')}}</p>
      </div>
      <br />
      @endif
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Pengaturan</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  @foreach ($guru as $key)
    <section class="content">
      <div class="row">
        <div style="margin-left: 15%" class="col-8">
          <div class="card">
            <form class="" action="{{route('pengaturan.update', $key->id)}}" method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="_method" value="PATCH">
              <input type="hidden" value="{{$key->users->foto}}" name="foto_lama" >
            <div class="card-header">
              <h4>Data Pribadi</h4>
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <center><img src="storage/app/public/upload_foto/{{auth::user()->foto}}" width="150px" class="img-circle elevation-2"></center>
                    </div>
                    <div class="form-group">
                      <label for="">Ganti Foto</label>
                      <input type="file" name="foto" value="{{ $key->foto}}" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="">Jenis Kelamin</label>
                      <div class="row">
                        <div class="col-6">
                          <input @if ($key->users->jk == 1) checked
                          @endif type="radio" name="jk" value="1"> Laki-laki
                        </div>
                        <div class="col-6">
                          <input @if ($key->users->jk == 2) checked
                          @endif type="radio" name="jk" value="2"> Perempuan
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="">Agama</label>
                      <select class="form-control" name="id_agama">
                        <option value="{{ $key->users->id_agama}}">{{ $key->users->agama->agama}}</option>
                        @foreach ($agama as $agm)
                          <option value="{{$agm->id}}">{{$agm->agama}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-8">
                    <div class="form-group">
                      <label for="nama">Nomer Induk Pengajar (NIP)</label>
                      <input type="text" name="nip" value="{{ $key->nip}}" required class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama Lengkap</label>
                      <input type="text" name="nama" value="{{$key->users->nama}}" required class="form-control">
                    </div>
                    <div class="row">
                      <div class="col-6">
                        <label for="nama">Tempat Lahir</label>
                        <input type="text" name="tempatlahir" value="{{$key->users->tempatlahir}}" required class="form-control">
                      </div>
                      <div class="col-6">
                        <label for="nama">Tanggal Lahir</label>
                        <input type="date" name="tanggallahir" value="{{$key->users->tanggallahir}}" required class="form-control">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="nama">Nomer Telepon</label>
                      <input type="number" name="nomertelepon" value="{{$key->users->telepon}}" required class="form-control">
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="">Alamat</label>
                      <textarea name="alamat" class="form-control" rows="5">{{$key->users->alamat}}</textarea>
                    </div>
                  </div>
                </div>
                <hr><h4>Pengaturan Akses</h4>
                <div class="row">

                  <div class="col-6">
                    <div class="form-group">
                      <label for="">Email</label>
                      <input type="text" name="email" value="{{$key->users->email}}" required class="form-control">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="">Password</label>
                      <input type="password" name="password" value="{{$key->users->password}}" required class="form-control">
                    </div>
                  </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="clearfix">

            </div>
            <div class="card-body">
              <center><button type="submit" name="submit" class="btn btn-success btn-lg" style="width : 50%">Simpan</button> </center
            </div>
          </form>
            <!-- /.card-body -->

          </div>
          <!-- /.card -->
        </div>
      </div><!-- /.row -->
    </section>
  @endforeach
  <!-- Main content -->

  <!-- /.content -->
</div>
@endsection
