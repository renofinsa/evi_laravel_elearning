@extends('master')
@section('title', Auth::user()->nama)

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      @if(\Session::has('alert'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{\Session::get('alert')}}</p>
      </div>
      <br />
      @endif
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Pengaturan</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  @foreach ($siswa as $key)
    <section class="content">
      <div class="row">
        <div style="margin-left: 15%" class="col-8">
          <div class="card">
            <form class="" action="{{route('pengaturan.update', $key->id)}}" method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="_method" value="PATCH">
              <input type="hidden" value="{{$key->user->foto}}" name="foto_lama" >
            <div class="card-header">
              <h4>Data Pribadi</h4>
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <center><img src="storage/app/public/upload_foto_siswa/{{auth::user()->foto}}" width="150px" class="img-circle elevation-2"></center>
                    </div>
                    <div class="form-group">
                      <label for="">Ganti Foto</label>
                      <input type="file" name="foto" value="{{ $key->foto}}" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="">Jenis Kelamin</label>
                      <div class="row">
                        <div class="col-6">
                          <input @if ($key->user->jk == 1) checked
                          @endif type="radio" name="jk" value="1"> Laki-laki
                        </div>
                        <div class="col-6">
                          <input @if ($key->user->jk == 2) checked
                          @endif type="radio" name="jk" value="2"> Perempuan
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="">Agama</label>
                      <select class="form-control" name="id_agama">
                        <option value="{{ $key->user->id_agama}}">{{ $key->user->agama->agama}}</option>
                        @foreach ($agama as $agm)
                          <option value="{{$agm->id}}">{{$agm->agama}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-8">
                    <div class="form-group">
                      <label for="nama">Nomer Induk Siswa (NIS)</label>
                      <input type="text" name="nis" value="{{ $key->nis}}" required class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama Lengkap</label>
                      <input type="text" name="nama" value="{{$key->user->nama}}" required class="form-control">
                    </div>
                    <div class="row">
                      <div class="col-6">
                        <label for="nama">Tempat Lahir</label>
                        <input type="text" name="tempatlahir" value="{{$key->user->tempatlahir}}" required class="form-control">
                      </div>
                      <div class="col-6">
                        <label for="nama">Tanggal Lahir</label>
                        <input type="date" name="tanggallahir" value="{{$key->user->tanggallahir}}" required class="form-control">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="nama">Nomer Telepon</label>
                      <input type="number" name="nomertelepon" value="{{$key->user->telepon}}" required class="form-control">
                    </div>
                    {{-- <div class="form-group">
                      <label for="nama">Kelas</label>
                      <select class="form-control" name="id_kelas">
                        <option value="{{$key->id_kelas}}">{{$key->kelas->kelas}}</option>
                        @foreach ($kelas->where('id', '<>', $key->id_kelas) as $kls)
                          <option value="{{$kls->id}}">{{$kls->kelas}}</option>
                        @endforeach
                      </select>
                    </div> --}}
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="">Alamat</label>
                      <textarea name="alamat" class="form-control" rows="5">{{$key->user->alamat}}</textarea>
                    </div>
                  </div>
                </div>
                <hr><h4>Pengaturan Akses</h4>
                <div class="row">

                  <div class="col-6">
                    <div class="form-group">
                      <label for="">Email</label>
                      <input type="text" name="email" value="{{$key->user->email}}" required class="form-control">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="">Password</label>
                      <input type="password" name="password" value="{{$key->user->password}}" class="form-control">
                      {{-- <input type="hidden" name="password_lama" value="{{$key->user->password}}" required class="form-control"> --}}
                    </div>
                  </div>
                </div>
                <hr><h4>Orang Tua</h4>
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <label for="">Nama Ayah</label>
                      <input type="text" name="namaayah" value="{{ $key->nama_ayah}}" class="form-control" required>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="">Nama Ibu</label>
                      <input type="text" name="namaibu" value="{{ $key->nama_ibu}}" class="form-control" required>
                    </div>
                  </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="clearfix">

            </div>
            <div class="card-body">
              <center><button type="submit" name="submit" class="btn btn-success btn-lg" style="width : 50%">Simpan</button> </center
            </div>
          </form>
            <!-- /.card-body -->

          </div>
          <!-- /.card -->
        </div>
      </div><!-- /.row -->
    </section>
  @endforeach
  <!-- Main content -->

  <!-- /.content -->
</div>
@endsection
