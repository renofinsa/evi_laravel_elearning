@extends('master2')
@section('title', 'Quiz')

@section('content')
<div class="content-wrapper" style="background-image: url('../dist/img/bg_out.jpg'">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      @if(\Session::has('alert'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{\Session::get('alert')}}</p>
      </div>
      <br />
      @endif
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">{{ $quiz->judul}}</h1>
        </div>

      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      @if (checkPermission(['siswa']))
        <div class="col-md-4">
          <div class="card">
            <div class="card-header">
              @foreach ($hasil->where('id_users','=',Auth::user()->id) as $nilai)
                  <h3 class="card-title">Nilai : {{$nilai->nilai}}  </h3>
                  <h3 class="card-title">Benar : {{$nilai->benar}}  </h3>
                  <h3 class="card-title">Salah : {{$nilai->salah}}  </h3>
                  <h3 class="card-title">Kosong : {{$nilai->kosong}}  </h3>
              @endforeach

            </div>
            <div class="clearfix"></div>
            <div class="card-body table-responsive p-3">
              <h5>Info :</h5>
              <p>{{ $quiz->info}}</p>
            </div>
            <a href="mulai/{{ $quiz->id}}" class="btn btn-success form-control">Mulai</a>




          </div>
        </div>
      @endif
      @if (checkPermission(['pengajar']))
        {{-- <div class="col-md-4">
          <div class="card">
            <div class="card-header">

            </div>
            <div class="clearfix"></div>
            <div class="card-body table-responsive p-3">
              Total Siswa :
            </div>
          </div>
        </div> --}}
      @endif
      <div  class="col-md-8">

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">{{ Auth::user()->nama}}  </h3>
          </div>
          <div class="clearfix"></div>
          <div class="card-body table-responsive p-3">
            <form class="" action="{{ route('komen.store') }}" method="post" enctype="multipart/form-data">
                @csrf
              <input type="hidden" class="form-control" name="idquiz" value="{{ $quiz->id}}">
              <div class="form-group">
                <input type="text" class="form-control" name="pesan" value="">
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-info form-control" name="submit">Kirim</button>
              </div>
            </form>
          </div>
        </div>
        <h2>Komentar</h2>
        @foreach ($quiz->komen()->latest()->get() as $komen )
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ $komen->users->nama}}  <span style="float: right">| {{ $komen->created_at->diffForHumans()}} |</span></h3>
            </div>
            <div class="clearfix"></div>
            <div class="card-body table-responsive p-3">
              <p>{{ $komen->pesan}}</p>
            </div>
          </div>
        @endforeach




      </div>





    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
@endsection
