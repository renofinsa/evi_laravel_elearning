@extends('master2')
@section('title', 'Quiz')

@section('content')
<div class="content-wrapper" >
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      @if(\Session::has('alert'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{\Session::get('alert')}}</p>
      </div>
      <br />
      @endif
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">{{ $quiz->judul}}</h1>
        </div>

      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      {{-- <div class="col-md-4">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Nilai : 0  </h3>
          </div>
          <div class="clearfix"></div>
          <div class="card-body table-responsive p-3">
            <h5>Info :</h5>
            <p>{{ $quiz->info}}</p>
          </div>
          <a href="mulai/{{ $quiz->id}}" class="btn btn-success form-control">Mulai</a>
        </div>
      </div> --}}
      <div style="margin-left : 20%" class="col-md-8">


        <?php $no = 0; ?>
        @foreach ($quiz->soal()->inRandomOrder()->get() as $soal )
          <?php $no++ ?>
          <form class="" action="{{ route('jawaban.store')}}" method="post">
            @csrf
            <input type="hidden" name="id[]" value="{{ $soal->id }}">
            {{-- <input type="hidden" name="jumlah" value="{{ $soal->count()->where('id_quiz','=','')}}"> --}}
            <input type="hidden" name="idquiz" value="{{ $quiz->id }}">
            <input type="hidden" name="jawabanbenar[{{$soal->id}}]" value="{{ $soal->jawabanbenar }}">

          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-md-1">
                  <h3>{{ $no}} </h3>
                </div>
                <div class="col-md-11">

                      <div class="row">

                          @if ($soal->gambar == 'noimage.jpg')

                          @elseif ($soal->gambar)
                            <div class="col-md-4">
                              <img src="../../../storage/app/public/upload_foto_soal/{{$soal->gambar}}" width="200px" style="margin-right : 10px">
                            </div>
                          @endif



                        <div class="col-md-8">
                          <h3 class="card-title">{{ $soal->soal}} </h3>
                        </div>
                      </div>


                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="card-body table-responsive p-3">
              <div class="form-group has-feedback">
                <input type="radio" name="pilihan[{{$soal->id}}]" value="a" style="margin-right: 10px"><label for="">A</label>
                <input type="text" name="jawaban1" class="form-control" value="{{ $soal->jawaban_a}}" readonly>
              </div>

              <div class="form-group has-feedback">
                <input type="radio" name="pilihan[{{$soal->id}}]" value="b" style="margin-right: 10px"><label for="">B</label>
                <input type="text" name="jawaban2" class="form-control" value="{{ $soal->jawaban_b}}" readonly>
              </div>

              <div class="form-group has-feedback">
                <input type="radio" name="pilihan[{{$soal->id}}]" value="c" style="margin-right: 10px"><label for="">C</label>
                <input type="text" name="jawaban3" class="form-control" value="{{ $soal->jawaban_c}}" readonly>
              </div>

              <div class="form-group has-feedback">
                <input type="radio" name="pilihan[{{$soal->id}}]" value="d" style="margin-right: 10px"><label for="">D</label>
                <input type="text" name="jawaban4" class="form-control" value="{{ $soal->jawaban_d}}" readonly>
              </div>

              <div class="form-group has-feedback">
                <input type="radio" name="pilihan[{{$soal->id}}]" value="e" style="margin-right: 10px"><label for="">E</label>
                <input type="text" name="jawaban5" class="form-control" value="{{ $soal->jawaban_e}}" readonly>
              </div>
            </div>
          </div>
        @endforeach

        <div class="form-group">
          <input class="btn btn-success form-control" type="submit" name="submit" value="Jawab" onclick="return confirm('Apakah Anda yakin dengan jawaban Anda?')">
        </div>

        </form>

      </div>





    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
@endsection
