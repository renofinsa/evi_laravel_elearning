@extends('master')
@section('title', 'Quiz')

@section('content')
<div class="content-wrapper" style="background-image: url('dist/img/bg_out.jpg'">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      @if(\Session::has('alert'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{\Session::get('alert')}}</p>
      </div>
      <br />
      @endif
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Tugas / Quiz</h1>
        </div>

      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      @foreach ($quiz as $qz)
        <div class="col-4">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ $qz->judul}}</h3>
              <p style="text-align : right">{{ $qz->created_at->diffForHumans() }}</p>
            </div>
            <div class="clearfix"></div>
            <div class="card-body table-responsive p-3">
              <h5>Info :</h5>
              <p>{{ $qz->info}}</p>
            </div>
            <a href="quizsiswa/{{$qz->idquiz}}" class="btn btn-success form-control">Lihat</a>
          </div>
        </div>
      @endforeach
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
@endsection
