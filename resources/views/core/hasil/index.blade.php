@extends('master1')
@section('title', $quiz->judul)

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      @if(\Session::has('alert'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{\Session::get('alert')}}</p>
      </div>
      <br />
      @endif
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">{{ $quiz->judul}}</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Index</h3>

            <div class="card-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                <a href="/get_hasil/{{$quiz->id}}" target="new_blank" >Download</a>
                  {{-- <a href="/" class="btn btn-success btn-sm" download target="_blank">Download</a> --}}
              </div>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="clearfix">

          </div>
          <div class="card-body table-responsive p-3">
            <table id="example1" class="table table-hover table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Nilai</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; ?>
              @foreach ($hasil as $hsl )
                <tr>
                  <td>{{ $no++}}</td>
                  <td>{{ $hsl->users->nama}}</td>
                  <td>{{ $hsl->nilai}}</td>
                  <td>
                    <button class="btn btn-danger btn-sm" type="button" name="button" data-toggle="modal" data-target="#delete_{{$hsl->id}}">Hapus Nilai</button>
                  </td>
                </tr>
                <!-- MODAL DELETE -->
                <div id="delete_{{$hsl->id}}" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Hapus Kelas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                        <form class="" action="{{route('hasil.destroy', $hsl->id)}}" method="post">
                          @csrf
                          <input type="hidden" name="_method" value="DELETE">
                          <p>Yakin Anda ingin nilai  {{$hsl->users->nama}}</p>
                          <div class="form-group">
                            <button type="submit" name="button" class="btn btn-danger btn-sm">Hapus</button>
                            <button type="button" name="button" class="btn btn-secondary btn-sm" data-dismiss=modal>Batal</button>
                          </div>

                        </form>
                      </div>
                    </div>

                  </div>
                </div>
                <!-- MODAL DELETE -->
              @endforeach



            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Kelas</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
            </table>
          </div>
          <!-- /.card-body -->

        </div>
        <!-- /.card -->
      </div>
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
@endsection
