@extends('master')
@section('title', 'Manajemen Pengajar')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      @if(\Session::has('alert'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{\Session::get('alert')}}</p>
      </div>
      <br />
      @endif
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Manajemen Pengajar</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Index</h3>

            <div class="card-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                  <button class="btn btn-success btn-sm" type="button" name="button" data-toggle="modal" data-target="#tambah">Tambah Pengajar</button>
              </div>
              <!-- MODAL TAMBAH -->
              <div id="tambah" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Tambah Pengajar</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('pengajar.store')}}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group has-feedback">
                          <label for="">Foto (Jpg,Png | Max 2000kb)</label>
                          <input required type="file" name="foto" value="lorem" class="form-control{{ $errors->has('foto') ? ' is-invalid' : '' }}"
                          placeholder="Joko Anwar">
                          @if ($errors->has('foto'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('foto') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nama Lengkap</label>
                          <input required style="text-transform : capitalize" type="text" name="nama" value="" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" required
                          placeholder="Joko Anwar">
                          @if ($errors->has('nama'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('nama') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">NIP (Nomer Induk Pengajar)</label>
                          <input required type="number" name="nip" value="" class="form-control{{ $errors->has('nis') ? ' is-invalid' : '' }}" required
                          placeholder="102030405060">
                          @if ($errors->has('nis'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('nis') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label required for="">Email</label>
                          <input type="email" name="email" value="" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required
                          placeholder="email@domain.com">
                          @if ($errors->has('email'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Kata Sandi</label>
                          <input type="text" name="kode" value="" class="form-control" required placeholder="masukan kata sandi kamu disini">
                          @if ($errors->has('kode'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('kode') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="agama">Agama</label>
                          <select class="form-control" name="id_agama" id="" required>
                            @foreach ($agama as $aa)
                              <option value="{{ $aa->id}}">{{ $aa->agama}}</option>
                            @endforeach
                          </select>

                                @if ($errors->has('id_agama'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('id_agama') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Tempat Lahir</label>
                          <input id="tempatlahir" type="text" class="form-control{{ $errors->has('tempatlahir') ? ' is-invalid' : '' }}" name="tempatlahir" required placeholder="Cth : Jakarta">
                          @if ($errors->has('tempatlahir'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('tempatlahir') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Tanggal Lahir</label>
                          <input id="tanggallahir" type="date" class="form-control{{ $errors->has('tanggallahir') ? ' is-invalid' : '' }}" name="tanggallahir" required >
                          @if ($errors->has('tanggallahir'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('tanggallahir') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Jenis Kelamin</label>
                          <div class="row">
                            <div class="col-md-6">
                              <input type="radio" name="jk" value="1" checked class="{{ $errors->has('gender') ? ' is-invalid' : '' }}"> Pria<br>
                            </div>
                            <div class="col-md-6">
                                <input type="radio" name="jk" value="2" class="{{ $errors->has('gender') ? ' is-invalid' : '' }}"> Wanita<br>
                            </div>
                          </div>
                          @if ($errors->has('jk'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('jk') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nomer Telepon</label>
                          <input type="number" name="nomertelepon"  class="form-control{{ $errors->has('nomertelepon') ? ' is-invalid' : '' }}" required placeholder="+62822XXXXX">
                          @if ($errors->has('nomertelepon'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('nomertelepon') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Alamat</label>
                          <textarea name="alamat" class="form-control{{ $errors->has('alamat') ? ' is-invalid' : '' }}" rows="5" cols="80" placeholder="masukan alamat kamu disini" required></textarea>
                          @if ($errors->has('alamat'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('alamat') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <button type="submit" name="button" class="btn btn-info">Simpan</button>
                          <button type="button" name="button" class="btn btn-danger" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                    <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                  </div>

                </div>
              </div>
              <!-- MODAL TAMBAH -->
            </div>
          </div>
          <!-- /.card-header -->
          <div class="clearfix">

          </div>
          <div class="card-body table-responsive p-3">
            <table id="example1" class="table table-hover table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>NIP</th>
                <th>Nama Lengkap</th>
                <th>Jenis Kelamin</th>
                <th>Nomer Telepon</th>
                <th>Foto Profil</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 0; ?>
              @foreach ($guru as $a)
              <?php $no++ ?>

              <tr>
                <td>{{ $no }}</td>
                <td>{{ $a->nip}}</td>
                <td>{{ $a->users->nama}}</td>
                @if($a->users->jk =='1')

                    <td>Pria</td>
                  @else
                    <td>Wanita</td>

                @endif
                <td>{{ $a->users->telepon}}</td>
                <td><img src="storage/app/public/upload_foto/{{$a->users->foto}}" width="100px"></td>
                <td>
                  <button class="btn btn-primary btn-sm" type="button" name="button" data-toggle="modal" data-target="#edit_{{$a->id}}">Ubah</button>
                  <button class="btn btn-danger btn-sm" type="button" name="button" data-toggle="modal" data-target="#delete_{{$a->id}}">Hapus</button>
                  <button class="btn btn-info btn-sm" type="button" name="button" data-toggle="modal" data-target="#lihat_{{$a->id}}">Lihat</button>
                </td>
              </tr>
              <!-- MODAL LIHAT -->
              <div id="lihat_{{$a->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">NIP : {{ $a->nip}}</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <center><img src="storage/app/public/upload_foto/{{$a->users->foto}}" width="150px"></center>
                          </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                              <h5>Nama : {{ $a->users->nama}}</h5>
                            </div>
                            <div class="form-group">
                              <h5>Jenis Kelamin :
                                @if($a->users->jk =='1')

                                    Pria
                                  @else
                                    Wanita

                                @endif

                              </h5>
                            </div>
                            <div class="form-group">
                              <h5>Agama : {{ $a->users->agama->agama}}</h5>
                            </div>
                        </div>
                      </div>
                      <hr>
                      <h5><b>Tempat, Tanggal Lahir :</b> <br>{{ $a->users->tempatlahir }}, {{ $a->users->tanggallahir}}</h5>
                      <h5><b>Nomer Telepon :</b> <br>{{ $a->users->telepon }}</h5>
                      <h5><b>Email : </b></br>{{ $a->users->email }}</h5>
                      <h5><b>Alamat : </b></br>{{ $a->users->alamat }}</h5>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL LIHAT -->
              <!-- MODAL EDIT -->
              <div id="edit_{{$a->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Ubah Data Pengajar</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('pengajar.update', $a->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group">
                          <center><img src="storage/app/public/upload_foto/{{$a->users->foto}}" width="100px"></center>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Foto (Jpg,Png | Max 2000kb)</label>
                          <input type="hidden" value="{{$a->users->foto}}" name="foto_lama">
                          <input type="file" name="foto" class="form-control{{ $errors->has('foto') ? ' is-invalid' : '' }}"
                          placeholder="Joko Anwar">
                          @if ($errors->has('foto'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('foto') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nama Lengkap</label>
                          <input style="text-transform : capitalize" type="text" name="nama" value="{{ $a->users->nama}}" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" required
                          placeholder="Joko Anwar">
                          @if ($errors->has('nama'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('nama') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">NIP (Nomer Induk Pengajar)</label>
                          <input type="number" name="nip" value="{{ $a->nip}}" class="form-control{{ $errors->has('nis') ? ' is-invalid' : '' }}" required
                          placeholder="102030405060">
                          @if ($errors->has('nis'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('nis') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Email</label>
                          <input type="email" name="email" value="{{ $a->users->email}}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required
                          placeholder="email@domain.com">
                          @if ($errors->has('email'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Kata Sandi</label>
                          <input type="password" name="password" value="{{ $a->users->password}}" class="form-control" required placeholder="masukan kata sandi kamu disini">
                          <input type="hidden" name="password_lama" value="{{ $a->users->password}}" class="form-control" required placeholder="masukan kata sandi kamu disini">

                        </div>
                        <div class="form-group has-feedback">
                          <label for="agama">Agama</label>
                          <select class="form-control" name="id_agama" id="" required>
                            <option  value="{{ $a->users->agama->id}}">{{ $a->users->agama->agama}}</option>
                            @foreach ($agama as $agamas)
                              <option value="{{ $agamas->id}}">{{ $agamas->agama}}</option>
                            @endforeach
                          </select>

                                @if ($errors->has('id_agama'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('id_agama') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Tempat Lahir</label>
                          <input value="{{ $a->users->tempatlahir}}" id="tempatlahir" type="text" class="form-control{{ $errors->has('tempatlahir') ? ' is-invalid' : '' }}" name="tempatlahir" required placeholder="Cth : Jakarta">
                          @if ($errors->has('tempatlahir'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('tempatlahir') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Tanggal Lahir</label>
                          <input value="{{ $a->users->tanggallahir}}" id="tanggallahir" type="date" class="form-control{{ $errors->has('tanggallahir') ? ' is-invalid' : '' }}" name="tanggallahir" required >
                          @if ($errors->has('tanggallahir'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('tanggallahir') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Jenis Kelamin</label>
                          <div class="row">
                            <div class="col-md-6">
                              <input type="radio" name="jk" value="1"
                              @if ($a->users->jk =='1' )
                                checked
                              @endif
                                class="{{ $errors->has('jk') ? ' is-invalid' : '' }}"> Pria<br>

                            </div>
                            <div class="col-md-6">
                                <input type="radio" name="jk" value="2"
                                 @if ($a->users->jk =='2' )
                                  checked
                                @endif
                                class="{{ $errors->has('jk') ? ' is-invalid' : '' }}"> Wanita<br>

                            </div>
                          </div>
                          @if ($errors->has('jk'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('jk') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nomer Telepon</label>
                          <input value="{{ $a->users->telepon}}" type="number" name="nomertelepon"  class="form-control{{ $errors->has('nomertelepon') ? ' is-invalid' : '' }}" required placeholder="+62822XXXXX">
                          @if ($errors->has('nomertelepon'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('nomertelepon') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Alamat</label>
                          <textarea  name="alamat" class="form-control{{ $errors->has('alamat') ? ' is-invalid' : '' }}" rows="5" cols="80" placeholder="masukan alamat kamu disini" required>{{ $a->users->alamat}}</textarea>
                          @if ($errors->has('alamat'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('alamat') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <button type="submit" name="button" class="btn btn-info">Simpan</button>
                          <button type="button" name="button" class="btn btn-danger" data-dismiss=modal>Batal</button>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL EDIT -->
              <!-- MODAL DELETE -->
              <div id="delete_{{$a->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Hapus Data Pengajar</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('pengajar.destroy', $a->id)}}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <p>Yakin Anda ingin menghapus data <b>{{$a->users->nama}}</b> </p>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-danger btn-sm">Hapus</button>
                          <button type="button" name="button" class="btn btn-secondary btn-sm" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL DELETE -->



              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>NIP</th>
                <th>Nama Lengkap</th>
                <th>Jenis Kelamin</th>
                <th>Nomer Telepon</th>
                <th>Foto Profil</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
            </table>
          </div>
          <!-- /.card-body -->

        </div>
        <!-- /.card -->
      </div>
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
@endsection
