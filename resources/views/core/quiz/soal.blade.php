@extends('master1')
@section('title',  $quiz->judul )

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      @if(\Session::has('alert'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{\Session::get('alert')}}</p>
      </div>
      <br />
      @endif
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Quiz : {{ $quiz->judul}}</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <ol class="breadcrumb">
      <li><a href="/quiz">Quiz</a> / </li>
      <li class="active"> {{$quiz->judul}}</li>
    </ol>
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Index</h3>

            <div class="card-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                  @if ($quiz->soal->count() == 10)
                    <button class="btn btn-danger btn-sm" type="button" name="button" data-toggle="modal" >Soal sudah maksimal</button>
                  @elseif ($quiz->soal->count() < 10)
                    <button class="btn btn-success btn-sm" type="button" name="button" data-toggle="modal" data-target="#tambah">Tambah Soal</button>
                  @endif

              </div>
              <!-- MODAL TAMBAH -->
              <div id="tambah" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Tambah Soal</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('soal.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="idquiz" value="{{ $quiz->id}}">
                        <div class="form-group has-feedback">
                          <label for="">Gambar</label>
                          <input type="file" name="foto" value="" class="form-control{{ $errors->has('judul') ? ' is-invalid' : '' }}">
                          @if ($errors->has('judul'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('judul') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Soal</label>
                          <textarea name="soal" rows="5" class="form-control" cols="80" required></textarea>
                          @if ($errors->has('judul'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('judul') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <h4 style="text-align: center">Jawaban</h4><hr>
                        </div>
                        <div class="form-group has-feedback">
                          <input type="radio" name="jawabanbenar" value="a" style="margin-right: 10px"><label for="">A</label>
                          <input type="text" name="jawaban1" class="form-control" value="" required>
                        </div>
                        <div class="form-group has-feedback">
                          <input type="radio" name="jawabanbenar" value="b" style="margin-right: 10px"><label for="">B</label>
                          <input type="text" name="jawaban2" class="form-control" value="" required>
                        </div>
                        <div class="form-group has-feedback">
                          <input type="radio" name="jawabanbenar" value="c" style="margin-right: 10px"><label for="">C</label>
                          <input type="text" name="jawaban3" class="form-control" value="" required>
                        </div>
                        <div class="form-group has-feedback">
                          <input type="radio" name="jawabanbenar" value="d" style="margin-right: 10px"><label for="">D</label>
                          <input type="text" name="jawaban4" class="form-control" value="" required>
                        </div>
                        <div class="form-group has-feedback">
                          <input type="radio" name="jawabanbenar" value="e" style="margin-right: 10px"><label for="">E</label>
                          <input type="text" name="jawaban5" class="form-control" value="" required>
                        </div>

                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-info">Simpan</button>
                          <button type="button" name="button" class="btn btn-danger" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                    <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                  </div>

                </div>
              </div>
              <!-- MODAL TAMBAH -->
            </div>
          </div>
          <!-- /.card-header -->
          <div class="clearfix">

          </div>
          <div class="card-body table-responsive p-3">
            <table id="example1" class="table table-hover table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Soal</th>
                <th>Jawaban</th>
                <th>Aksi</th>
            </thead>
            <tbody>
              <?php $no = 0;?>
              @foreach ($quiz->soal()->get() as $s)
                <?php $no++ ?>
                <tr>
                  <td>{{ $no}}</td>
                  <td>{{ $s->soal}}</td>
                  <td><button class="btn btn-warning btn-sm" type="button" name="button" data-toggle="modal" data-target="#lihat{{$s->id}}"><i class="nav-icon fa fa-eye"></i> </button></td>
                  <td>
                    <button class="btn btn-primary btn-sm" type="button" name="button" data-toggle="modal" data-target="#edit_{{$s->id}}">Ubah</button>
                    <button class="btn btn-danger btn-sm" type="button" name="button" data-toggle="modal" data-target="#delete_{{$s->id}}">Hapus</button>
                  </td>
                </tr>

              {{-- lihat jawaban --}}
              <div id="lihat{{$s->id}}" class="modal fade" role="dialog" >
                <div class="modal-dialog" >
                  <div class="modal-content" style="width : 720px; margin-left : -100px">
                    <div class="modal-header">
                      @if ($s->gambar == 'noimage.jpg')

                      @elseif ($s->gambar)
                        <img src="storage/app/public/upload_foto_soal/{{$s->gambar}}" width="200px">
                      @endif

                      <h4 class="modal-title">{{ $s->soal}}</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <ul class="list-group">
                        <div class="form-group has-feedback">
                          <input @if ($s->jawabanbenar == 'a') checked @endif type="radio" name="jawabanbenar" value="a" style="margin-right: 10px"><label for="">A</label>
                          <input type="text" name="jawaban1" class="form-control" value="{{ $s->jawaban_a}}" readonly required>
                        </div>
                        <div class="form-group has-feedback">
                          <input @if ($s->jawabanbenar == 'b') checked @endif type="radio" name="jawabanbenar" value="b" style="margin-right: 10px"><label for="">B</label>
                          <input type="text" name="jawaban2" class="form-control" value="{{ $s->jawaban_b}}" readonly required>
                        </div>
                        <div class="form-group has-feedback">
                          <input @if ($s->jawabanbenar == 'c') checked @endif type="radio" name="jawabanbenar" value="c" style="margin-right: 10px"><label for="">C</label>
                          <input type="text" name="jawaban3" class="form-control" value="{{ $s->jawaban_c}}" readonly required>
                        </div>
                        <div class="form-group has-feedback">
                          <input @if ($s->jawabanbenar == 'd') checked @endif type="radio" name="jawabanbenar" value="d" style="margin-right: 10px"><label for="">D</label>
                          <input type="text" name="jawaban4" class="form-control" value="{{ $s->jawaban_d}}" readonly required>
                        </div>
                        <div class="form-group has-feedback">
                          <input @if ($s->jawabanbenar == 'e') checked @endif type="radio" name="jawabanbenar" value="e" style="margin-right: 10px"><label for="">E</label>
                          <input type="text" name="jawaban5" class="form-control" value="{{ $s->jawaban_e}}" readonly required>
                        </div>

                      </ul>





                    </div>
                  </div>

                </div>
              </div>
              <!-- lihat jawaban -->


              <!-- MODAL EDIT -->
              <div id="edit_{{$s->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Ubah Kelas</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                      <form class="" action="{{ route('soal.update', $s->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group has-feedback">
                          <label for="">Gambar</label>
                          <input type="file" name="gambar" value="{{ $s->gambar }}" class="form-control{{ $errors->has('judul') ? ' is-invalid' : '' }}">

                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Soal</label>
                          <textarea name="soal" rows="5" class="form-control" cols="80" required>{{ $s->soal}}</textarea>
                        </div>
                        <div class="form-group has-feedback">
                          <h4 style="text-align: center">Jawaban</h4><hr>
                        </div>
                        <div class="form-group has-feedback">
                          <input @if ($s->jawabanbenar == 'a') checked @endif type="radio" name="jawabanbenar" value="a" style="margin-right: 10px"><label for="">A</label>
                          <input type="text" name="jawaban1" class="form-control" value="{{ $s->jawaban_a}}" required>
                        </div>
                        <div class="form-group has-feedback">
                          <input @if ($s->jawabanbenar == 'b') checked @endif type="radio" name="jawabanbenar" value="b" style="margin-right: 10px"><label for="">B</label>
                          <input type="text" name="jawaban2" class="form-control" value="{{ $s->jawaban_b}}" required>
                        </div>
                        <div class="form-group has-feedback">
                          <input @if ($s->jawabanbenar == 'c') checked @endif type="radio" name="jawabanbenar" value="c" style="margin-right: 10px"><label for="">C</label>
                          <input type="text" name="jawaban3" class="form-control" value="{{ $s->jawaban_c}}" required>
                        </div>
                        <div class="form-group has-feedback">
                          <input @if ($s->jawabanbenar == 'd') checked @endif type="radio" name="jawabanbenar" value="d" style="margin-right: 10px"><label for="">D</label>
                          <input type="text" name="jawaban4" class="form-control" value="{{ $s->jawaban_d}}" required>
                        </div>
                        <div class="form-group has-feedback">
                          <input @if ($s->jawabanbenar == 'e') checked @endif type="radio" name="jawabanbenar" value="e" style="margin-right: 10px"><label for="">E</label>
                          <input type="text" name="jawaban5" class="form-control" value="{{ $s->jawaban_e}}" required>
                        </div>

                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-success btn-sm">Simpan</button>
                          <button type="button" name="button" class="btn btn-warning btn-sm" data-dismiss=modal>Batal</button>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL EDIT -->
              <!-- MODAL DELETE -->
              <div id="delete_{{$s->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Hapus Soal</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{ route('soal.destroy', $s->id)}}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <p>Yakin Anda ingin menghapus soal ini ? </p>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-danger btn-sm">Hapus</button>
                          <button type="button" name="button" class="btn btn-secondary btn-sm" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL DELETE -->
            @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Soal</th>
                <th>Jawaban</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
            </table>
          </div>
          <!-- /.card-body -->

        </div>
        <!-- /.card -->
      </div>
      <div class="row" style="margin-left : 5%">
        <div class="col-12">
          <div class="form-group">
            <h3>Pesan : </h3>
            <p>Jika soal sudah 10 (terpenuhi), maka tombol "Tambah Quiz" tidak aktif</p>
          </div>
        </div>
      </div>
    </div><!-- /.row -->
  </section>

  <!-- /.content -->
</div>
@endsection
