@extends('master')
@section('title', 'Quiz')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      @if(\Session::has('alert'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{\Session::get('alert')}}</p>
      </div>
      <br />
      @endif
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Quiz</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Index</h3>

            <div class="card-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                  @if (checkPermission(['pengajar']))
                    <button class="btn btn-success btn-sm" type="button" name="button" data-toggle="modal" data-target="#tambah">Tambah Quiz</button>
                  @endif
              </div>
              <!-- MODAL TAMBAH -->
              <div id="tambah" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Tambah Quiz</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('quiz.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group has-feedback">
                          <label for="">Judul</label>
                          <input type="text" name="judul" value="" class="form-control{{ $errors->has('judul') ? ' is-invalid' : '' }}" required>
                          @if ($errors->has('judul'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('judul') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="kelas">Kelas & Mata Pelajaran</label>
                          <select class="form-control{{ $errors->has('idrelasi') ? ' is-invalid' : '' }}" name="idrelasi" id="" required>
                            @foreach ($gurumatpel as $gm)
                              <option value="{{ $gm->id}}">{{ $gm->kelas->kelas}} | {{ $gm->matpel->matpel}}</option>
                            @endforeach
                          </select>

                                @if ($errors->has('idrelasi'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('idrelasi') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Info</label>
                          <textarea name="info" rows="5" class="form-control" placeholder="tulis informasi mengenai quiz disini ...." required></textarea>
                          @if ($errors->has('file'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('file') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-info">Simpan</button>
                          <button type="button" name="button" class="btn btn-danger" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                    <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                  </div>

                </div>
              </div>
              <!-- MODAL TAMBAH -->
            </div>
          </div>
          <!-- /.card-header -->
          <div class="clearfix">

          </div>
          <div class="card-body table-responsive p-3">
            <table id="example1" class="table table-hover table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Keterangan</th>
                <th>Tanggal Posting</th>
                <th>Pembuat</th>
                @if (checkPermission(['pengajar']))
                  <th>Aksi</th>
                @endif
            </thead>
            <tbody>
              <?php $no = 0;
                $color = 'red';
              ?>
              @foreach ($quiz as $key)
                <?php $no++ ?>
                <tr>
                  @if ($key->status == 0)
                      <td style="background : red">{{ $no }}</td>
                  @elseif ($key->status == 1)
                    <td>{{ $no }}</td>
                  @endif



                  <td>{{ $key->judul}}</td>
                  <td>{{ $key->relasi->kelas->kelas}} | {{ $key->relasi->matpel->matpel}}</td>

                  <td>{{ date('d F Y', strtotime($key->created_at)) }}</td>
                  <td>{{$key->users->nama}}</td>
                  @if (checkPermission(['pengajar']))
                    <td>
                      {{-- @if($key->id == Auth::user()->id) --}}
                      <button class="btn btn-primary btn-sm" type="button" name="button" data-toggle="modal" data-target="#edit_{{$key->id}}">Ubah</button>
                      <button class="btn btn-danger btn-sm" type="button" name="button" data-toggle="modal" data-target="#delete_{{$key->id}}">Hapus</button>
                      <a class="btn btn-info btn-sm" href="quiz/{{$key->id}}">Buat Soal</a>
                      <a class="btn btn-default btn-sm" href="quizsiswa/{{$key->id}}">Forum</a>
                      <a style="color : #fff" class="btn btn-warning btn-sm" href="hasil/{{$key->id}}">Nilai</a>
                      {{-- @else --}}
                      {{-- <button class="btn btn-primary btn-sm" type="button" name="button" data-toggle="modal" data-target="#edit_{{$key->id}}">Ubah</button>
                      <button class="btn btn-danger btn-sm" type="button" name="button" data-toggle="modal" data-target="#delete_{{$key->id}}">Hapus</button> --}}
                      {{-- @endif --}}
                    </td>
                  @endif

                </tr>



              <!-- MODAL EDIT -->
              <div id="edit_{{$key->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Ubah Data</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('quiz.update', $key->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group has-feedback">
                          <label for="">Judul</label>
                          <input type="text" name="judul" value="{{ $key->judul}}" class="form-control{{ $errors->has('judul') ? ' is-invalid' : '' }}" required>
                          @if ($errors->has('judul'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('judul') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="Relasi">Kelas & Mata Pelajaran</label>
                          <select class="form-control{{ $errors->has('idrelasi') ? ' is-invalid' : '' }}" name="idrelasi" id="" required>
                            <option value="{{ $key->id_relasi}}">{{ $key->relasi->kelas->kelas}} | {{ $key->relasi->matpel->matpel}} </option>
                            @foreach ($gurumatpel as $gm)
                              <option value="{{ $gm->id}}">{{ $gm->kelas->kelas}} | {{ $gm->matpel->matpel}}</option>
                            @endforeach
                          </select>

                                @if ($errors->has('idrelasi'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('idrelasi') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Info</label>
                          <textarea name="info" rows="5" class="form-control" placeholder="tulis informasi mengenai quiz disini ....">{{ $key->info}}</textarea>
                          @if ($errors->has('file'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('file') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Jenis Kelamin</label>
                          <div class="row">
                            <div class="col-md-6">
                              <input type="radio" name="status" value="1"
                              @if ($key->status =='1' )
                                checked
                              @endif
                                class="{{ $errors->has('status') ? ' is-invalid' : '' }}"> Aktif<br>

                            </div>
                            <div class="col-md-6">
                                <input type="radio" name="status" value="0"
                                 @if ($key->status =='0' )
                                  checked
                                @endif
                                class="{{ $errors->has('status') ? ' is-invalid' : '' }}"> Tidak Aktif<br>

                            </div>
                          </div>
                          @if ($errors->has('jk'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('jk') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-success btn-sm">Simpan</button>
                          <button type="button" name="button" class="btn btn-warning btn-sm" data-dismiss=modal>Batal</button>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL EDIT -->
              <!-- MODAL DELETE -->
              <div id="delete_{{$key->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Hapus Materi</h4>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('quiz.destroy', $key->id)}}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <p>Yakin Anda ingin menghapus materi {{$key->judul}}</p>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-danger btn-sm">Hapus</button>
                          <button type="button" name="button" class="btn btn-secondary btn-sm" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL DELETE -->
            @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Keterangan</th>
                <th>Tanggal Posting</th>
                <th>Pembuat</th>
                  @if (checkPermission(['pengajar']))
                    <th>Aksi</th>
                  @endif

              </tr>
            </tfoot>
            </table>

          </div>
          <!-- /.card-body -->

        </div>

        <!-- /.card -->
      </div>
      <div class="row" style="margin-left : 5%">
        <div class="col-12">
          <div class="form-group">
            <h3>Pesan : </h3>
            <p>Jika soal quiz kurang dari 10 maka Quiz tidak dapat aktif</p>
          </div>
        </div>
      </div>
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
@endsection
