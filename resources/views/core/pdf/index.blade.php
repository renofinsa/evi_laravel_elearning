<html>
<head>
  <title>Hasil</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<body>

<div class="container">

    <div class="col-md-6" style="float : left">
        <img src="dist/img/logo.png" alt="Logo SMAN 25" style="width: 150px"/>
    </div>
    @foreach ($quiz as $key)
      <div class="row" style="margin-left :30%">
          <p style="align : right"><b>Nama Guru : </b> {{$key->users->nama}}</p>
          <p style="align : right"><b>Mata Pelajaran : </b>{{$key->relasi->matpel->matpel}}</p>
          <p style="align : right"><b>Semester : </b>{{$key->relasi->semester->semester}}</p>
          <p style="align : right"><b>Kelas: </b>{{$key->relasi->kelas->kelas}}</p>
      </div>
    @endforeach



  <div class="panel panel-default" style="margin-top : 5%">
    <div class="panel-heading">
      <h4>Nilai Siswa</h4>
    </div>
    <div class="panel panel-body">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Siswa</th>
            <th>Nilai</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($hasil as $nilai)
            <tr>
              <td>{{$no++}}</td>
              <td>{{$nilai->users->nama}}</td>
              <td>{{$nilai->nilai}}</td>
            </tr>
          @endforeach

        </tbody>
      </table>
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
</body>
</html>
