@extends('master')
@section('title', 'Manajemen Materi')

@section('content')
<div class="content-wrapper" style="background-image: url('dist/img/bg_out.jpg'">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      @if(\Session::has('alert'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{\Session::get('alert')}}</p>
      </div>
      <br />
      @endif
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Materi</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      @foreach ($materi as $key)
      <div class="col-3">
        <div class="card">
          <div class="card-header">


                <h5>{{$key->judul}}</h5>
                <hr>
                {{-- <center><a href="storage/app/public/upload_file_materi/{{$key->nama_file}}" target="_blank" class="btn btn-info"><i class="fa fa-download"></i> Download</a></center> --}}
                <center><a href="storage/app/public/upload_file_materi/{{$key->nama_file}}" target="_blank" download class="btn btn-info"><i class="fa fa-download"></i> Download</a></center>



          </div>
          <!-- /.card-header -->
          <div class="clearfix">

          </div>

          <!-- /.card-body -->

        </div>
        <!-- /.card -->
      </div>
      @endforeach
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
@endsection
