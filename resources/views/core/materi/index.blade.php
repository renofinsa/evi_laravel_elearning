@extends('master')
@section('title', 'Manajemen Materi')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      @if(\Session::has('alert'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{\Session::get('alert')}}</p>
      </div>
      <br />
      @endif
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Manajemen Materi</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Index</h3>

            <div class="card-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                  <button class="btn btn-success btn-sm" type="button" name="button" data-toggle="modal" data-target="#tambah">Tambah Materi</button>
              </div>
              <!-- MODAL TAMBAH -->
              <div id="tambah" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Tambah Materi</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('materi.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group has-feedback">
                          <label for="">Judul</label>
                          <input type="text" name="judul" value="" class="form-control{{ $errors->has('judul') ? ' is-invalid' : '' }}" required>
                          @if ($errors->has('judul'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('judul') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="kelas">Kelas & MataPelajaran</label>
                          <select class="form-control{{ $errors->has('idrelasi') ? ' is-invalid' : '' }}" name="idrelasi" id="" required>
                          
                            @foreach ($gurumatpel as $gm)
                              <option value="{{ $gm->id}}">{{ $gm->kelas->kelas}} | {{ $gm->matpel->matpel}}</option>
                            @endforeach
                          </select>

                                @if ($errors->has('idrelasi'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('idrelasi') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group has-feedback">
                          <label for="">File</label>
                          <input type="file" name="nama_file" value="" class="form-control {{ $errors->has('file') ? ' is-invalid' : '' }}" required>
                          @if ($errors->has('file'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('file') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-info">Simpan</button>
                          <button type="button" name="button" class="btn btn-danger" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                    <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                  </div>

                </div>
              </div>
              <!-- MODAL TAMBAH -->
            </div>
          </div>
          <!-- /.card-header -->
          <div class="clearfix">

          </div>
          <div class="card-body table-responsive p-3">
            <table id="example1" class="table table-hover table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Keterangan</th>
                <th>Nama File</th>
                <th>Tanggal Posting</th>
                <th>Pembuat</th>
                <th>Aksi</th>
            </thead>
            <tbody>
              <?php $no = 0; ?>
              @foreach($materi as $m)
              <?php $no++ ?>
              <tr>
                <td>{{$no}}</td>
                <td>{{$m->judul}}</td>
                <td>{{ $m->relasi->kelas->kelas}} | {{ $m->relasi->matpel->matpel}}</td>
                <td>{{$m->nama_file}}</td>
                <td>{{ date('d F Y', strtotime($m->created_at)) }}</td>
                <td>{{$m->users->nama}}</td>
                <td>


                  <button class="btn btn-primary btn-sm" type="button" name="button" data-toggle="modal" data-target="#edit_{{$m->id}}">Ubah</button>
                  <button class="btn btn-danger btn-sm" type="button" name="button" data-toggle="modal" data-target="#delete_{{$m->id}}">Hapus</button>

                </td>
              </tr>

              <!-- MODAL EDIT -->
              <div id="edit_{{$m->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Ubah Materi</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('materi.update', $m->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group has-feedback">
                          <label for="">Judul</label>
                          <input type="text" name="judul" value="{{$m->judul}}" class="form-control{{ $errors->has('idrelasi') ? ' is-invalid' : '' }}" required>
                          @if ($errors->has('judul'))
                              <span class="invalid-feedback">
                                  <strong>{{ $errors->first('judul') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group has-feedback">
                          <label for="kelas">Pilih Kelas</label>
                          <select class="form-control{{ $errors->has('idrelasi') ? ' is-invalid' : '' }}" name="idrelasi" id="" required>
                            <option value="{{ $m->id_relasi}}">{{ $m->relasi->kelas->kelas}} | {{ $m->relasi->matpel->matpel}}</option>
                            @foreach ($gurumatpel as $gm)
                              <option value="{{ $gm->id}}">{{ $gm->kelas->kelas}} | {{ $gm->matpel->matpel}}</option>
                            @endforeach
                          </select>


                        </div>
                        <div class="form-group has-feedback">
                          <label for="">File</label>
                          <input type="hidden" value="{{$m->nama_file}}" name="file_lama">
                          <input type="file" name="nama_file" class="form-control{{ $errors->has('nama_file') ? ' is-invalid' : '' }}">
                          @if ($errors->has('nama_file'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('nama_file') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-success btn-sm">Simpan</button>
                          <button type="button" name="button" class="btn btn-warning btn-sm" data-dismiss=modal>Batal</button>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL EDIT -->
              <!-- MODAL DELETE -->
              <div id="delete_{{$m->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Hapus Materi</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="{{route('materi.destroy', $m->id)}}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <p>Yakin Anda ingin menghapus materi {{$m->judul}}</p>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-danger btn-sm">Hapus</button>
                          <button type="button" name="button" class="btn btn-secondary btn-sm" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL DELETE -->
            @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Keterangan</th>
                <th>Nama File</th>
                <th>Tanggal Posting</th>
                <th>Pembuat</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
            </table>
          </div>
          <!-- /.card-body -->

        </div>
        <!-- /.card -->
      </div>
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
@endsection
