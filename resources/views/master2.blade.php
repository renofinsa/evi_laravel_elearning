<!DOCTYPE html>
<html>
<head>
  <title>@yield('title')</title>
  @include('partial.head')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  @include('partial.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('partial.sidebar2')

  <!-- Content Wrapper. Contains page content -->
  @yield('content')
  <!-- /.content-wrapper -->
  @include('partial.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@include('partial.script')
</body>
</html>
