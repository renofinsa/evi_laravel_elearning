
## Requirement
- XAMPP ver php 7.2
- Visual Studio Code (IDE)
- Chrome Browser

## Cara Install

- Clone / Download Aplikasi melalui link di atas
- Buat database di XAMPP
- Buka File Project
- Setup .Env, ganti nama database sesuai yang kamu buat
- Buka terminal/ commend prompt lalu ketik "php artisan migrate"
- Jika berhasil. database akan genrate secara otomatis
- Lalu ketik lagi di terminal/ commend prompt "php artisan db:seed", untuk membuat akses
- Jika berhasil, kembali ke terminal/ commend prompt lalu ketik "php artisan serv", untuk menjalankan project

