<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Materi;

class matapelajaran extends Model
{
  protected $table = 'matapelajarans';

  protected $fillable = [
    'matpel'
  ];

    public function materi(){
    return $this->hasMany(Materi::class);
  }
}
