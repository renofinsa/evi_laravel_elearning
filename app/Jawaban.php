<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jawaban extends Model
{
    //

    protected $table = 'jawabans';

    protected $fillable = [
      'id_soal',
      'jawaban',
      'status'

    ];

    public function soal(){
      return $this->belongsTo(Soal::class,'id_soal');
    }
}
