<?php

namespace App;
use App\Siswa;
use App\Materi;
use Illuminate\Database\Eloquent\Model;

class kelas extends Model
{

    protected $table = 'kelas';

    protected $fillable = [
      'kelas',
      'ruangan'
    ];


    public function siswa(){
      return $this->hasOne(Siswa::class);
    }
    public function materi(){
    return $this->hasMany(Materi::class);
  }

}
