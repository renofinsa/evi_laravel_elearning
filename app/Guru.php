<?php

namespace App;
use App\Kelas;
use App\Agama;
use App\User;
use Illuminate\Database\Eloquent\Model;

class guru extends Model
{
  protected $table = 'gurus';

  protected $fillable = [
    'id_users',
    'nip'
  ];


      public function users(){
      return $this->belongsTo(User::class,'id_users');
      // return $this->hasOne('nama_model','id_relasi_dari_model_yang_dituju');
    }
    //   public function gurumatpel(){
    //   return $this->hasMany(GuruMatpel::class);
    //   // return $this->hasOne('nama_model','id_relasi_dari_model_yang_dituju');
    // }
}
