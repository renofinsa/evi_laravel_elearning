<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuruMatpel extends Model
{
    protected $table = 'guru_matpels';

    protected $fillable = [
      'id_guru',
      'id_matpel',
      'id_kelas',
      'id_semester',
      'status'
    ];

    public function users(){
    return $this->belongsTo(User::class,'id_guru');
    // return $this->hasOne('nama_model','id_relasi_dari_model_yang_dituju');
  }
    public function matpel(){
    return $this->belongsTo(MataPelajaran::class,'id_matpel');
    // return $this->hasOne('nama_model','id_relasi_dari_model_yang_dituju');
  }
    public function kelas(){
    return $this->belongsTo(Kelas::class,'id_kelas');
    // return $this->hasOne('nama_model','id_relasi_dari_model_yang_dituju');
  }
  public function semester(){
    return $this->belongsTo(Semester::class,'id_semester');
    // return $this->hasOne('nama_model','id_relasi_dari_model_yang_dituju');
  }
  public function quiz(){
    return $this->hasMany(Quiz::class,'id_relasi');
  }

}
