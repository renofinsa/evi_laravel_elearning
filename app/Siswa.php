<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Siswa;
use App\Kelas;
use App\Agama;
use App\User;
class siswa extends Model
{
    protected $table = 'siswas';

    protected $fillable = [
      'id_users',
      'nis',
      'nama_ayah',
      'nama_ibu',
      'id_kelas'
    ];

    public function kelas(){
    return $this->belongsTo(Kelas::class,'id_kelas');
    // return $this->hasOne('nama_model','id_relasi_dari_model_yang_dituju');
  }
    public function user(){
    return $this->belongsTo(User::class,'id_users');
    // return $this->hasOne('nama_model','id_relasi_dari_model_yang_dituju');
  }


}
