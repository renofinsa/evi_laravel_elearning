<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{
    protected $table = 'agamas';



    public function users(){
      return $this->hasOne(User::class);
    }

    protected $fillable = [
      'agama'
    ];
}
