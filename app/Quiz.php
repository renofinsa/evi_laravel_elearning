<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quiz extends Model
{
    protected $table = 'quizzes';

    protected $fillable = [
      'id_relasi',
      'id_pembuat',
      'judul',
      'info',
      'status'
    ];

    public function relasi(){
      return $this->belongsTo(GuruMatpel::class,'id_relasi');
    }
    public function users(){
      return $this->belongsTo(User::class,'id_pembuat');
    }
    public function soal(){
      return $this->hasMany(Soal::class,'id_quiz');
    }
    public function komen(){
      return $this->hasMany(Komen::class,'id_quiz');
    }
    public function hasil(){
      return $this->hasMany(JawabanTerpilih::class,'id_quiz');
    }


}
