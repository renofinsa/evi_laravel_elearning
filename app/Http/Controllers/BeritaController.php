<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\User;
use Auth;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }
    public function index()
    {
      $user = User::all();
      $berita = Berita::whereIn('id_pembuat', [1,Auth::user()->id])->get();
      return view('core.berita.index', compact('berita', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $berita = new Berita;
      $berita->judul = $request->get('judul');
      $berita->isi = $request->get('isi');
      $berita->id_pembuat = Auth::user()->id;
      // $berita->users()->create(array_merge
      //   ($request->only('nama'),
      //   ['id_pembuat' => auth()->id()]
      // ));

      $berita->save();

      return redirect('berita')->with('alert','Berhasil menyimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $berita = Berita::find($id);
      $berita->judul = $request->get('judul');
      $berita->isi = $request->get('isi');
      $berita->id_pembuat = Auth::user()->id;
      $berita->save();

      return redirect('berita')->with('alert','Berhasil mengubah data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $berita = Berita::find($id);
      $berita->delete();

      return redirect('berita')->with('alert','Berhasil menghapus data');
    }
}
