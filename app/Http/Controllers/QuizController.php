<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MataPelajaran;
use App\GuruMatpel;
use App\Soal;
use App\Quiz;
use App\Guru;
use App\Kelas;
use App\User;
use App\Komen;
use Auth;
class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }
    public function index()
    {

      if (Auth::user()->level == 1) {
        $gurumatpel = GuruMatpel::all();
        $kelas = Kelas::all();
        $matpel = MataPelajaran::all();
        $quiz = Quiz::all();
        $soal = Soal::all();
        // $jawaban   = Jawaban::all();
        return view('core.quiz.index',compact('quiz','kelas','matpel','gurumatpel','soal'));
      }else{
        $gurumatpel = GuruMatpel::where('id_guru', '=',Auth::user()->id )->get();
        $kelas = Kelas::all();
        $matapelajaran = MataPelajaran::all();
        $matpel = MataPelajaran::all();
        $quiz = Quiz::where('id_pembuat', '=',Auth::user()->id )->get();
        return view('core.quiz.index',compact('quiz','kelas','matpel','gurumatpel','matapelajaran'));
      }




    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      Quiz::create([
        'id_pembuat' => Auth::user()->id,
        'id_relasi' =>request('idrelasi'),
        'judul' => request('judul'),
        'info' => request('info'),
        // 'durasi' => request('durasi'),
        'status' => (0)
      ]);
      return redirect()->back()->with('alert', 'Post Ditambahkan');

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $quiz = Quiz::find($id);
      $soal = Soal::find($id);
      $komen = Komen::find($id);

      return view('core.quiz.soal',compact('quiz','soal','komen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cek = Soal::where('id_quiz','=',$id)->count();

        if ($cek == 10) {

          $status = $request->get('status');

          $up = Quiz::find($id);
          $up->id_relasi=$request->get('idrelasi');
          $up->judul = $request->get('judul');
          $up->info = $request->get('info');
          $up->status = $status;
          $up->save();



          return redirect()->back()->with('alert', 'Post Berhasil Diubah & Quiz sudah dapat diaktifkan');

        }else{
          $status = '0';


          $up = Quiz::find($id);
          $up->id_relasi=$request->get('idrelasi');
          $up->judul = $request->get('judul');
          $up->info = $request->get('info');
          $up->status = $status;
          $up->save();

          return redirect()->back()->with('alert', 'Post Berhasil Diubah & Soal Kurang dari 10');
        }




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quiz = Quiz::find($id);
        $quiz->delete();

        return redirect()->back()->with('alert', 'Post Berhasil Diubah');

    }



}
