<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.berita');
    }
    public function allAdmin()
    {
        //dd('Access All Admin');
        return view('dashboard.dashboard_admin');
    }
    public function allPengajar()
    {
        //dd('Access all Pengajar');
        return view('dashboard.dashboard_pengajar');
    }
    public function allSiswa()
    {
        //dd('Access all Siswa');
        return view('dashboard.dashboard_siswa');
    }
}
