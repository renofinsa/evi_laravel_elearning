<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\Kelas;
use App\Agama;
use App\User;
//use App\Auth;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }
    public function index()
    {
      // $user = User::pluck('email', 'password');
      $user = User::all();
      $kelas = Kelas::all();
      $agama = Agama::all();
      $siswa = Siswa::all();

      return view('core.siswa.index', compact('kelas','agama','user','siswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(array $data)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
      // dd(siswa);
      // $validator = Validator::make($request->all(), [
      //     'email' => 'required',
      //     'password' => 'required',
      // ]);
      //
      // if ($validator->fails()) {
      //     return redirect('#tambah')
      //                 ->withErrors($validator)
      //                 ->withInput();
      // }

      //handle upload file
      if($request->hasFile('foto')){
        $filenameWithExt = $request->file('foto')->getClientOriginalName();

        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

        $extension = $request->file('foto')->getClientOriginalExtension();
        $file_store_name = $filename.'_'.time().'.'.$extension;
        $path = $request->file('foto')->storeAs('public/upload_foto_siswa', $file_store_name);
      }else{
        $file_store_name = 'noimage.jpg';
      }

            $id = User::create([
               'nama' => request('nama'),
               'email' => request('email'),
               'password' => Hash::make(request('kode')),
               'level' => ('2'),
               'tempatlahir' => request('tempatlahir'),
               'tanggallahir' => request('tanggallahir'),
               'jk' => request('jk'),
               'telepon' => request('nomertelepon'),
               'alamat' => request('alamat'),
               'foto' => $file_store_name,
               'id_agama' => request('id_agama')


           ]);

            Siswa::create([
             'id_users' => $id->id,
             'nis' => request('nis'),
             'id_kelas' => request('id_kelas'),
             'nama_ayah' => request('namaayah'),
             'nama_ibu' => request('namaibu')

           ]);
           return redirect()->back()->with('alert', 'Berhasil');
          









    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if($request->hasFile('foto')){
          $filenameWithExt = $request->file('foto')->getClientOriginalName();

          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

          $extension = $request->file('foto')->getClientOriginalExtension();
          $file_store_name = $filename.'_'.time().'.'.$extension;
          $path = $request->file('foto')->storeAs('public/upload_foto_siswa', $file_store_name);
        }else{
          $file_store_name = $request->get('foto_lama');
        }

        if($request->get('password') == $request->get('password_lama')){
          $pass = $request->get('password');
        }else{
          $pass = Hash::make($request->get('password'));
        }

      $cek = $request->get('email');
      $email_lama = $request->get('email_lama');


      if ($cek == $email_lama) {
        $up = Siswa::find($id);
        $up->nis = $request->get('nis');
        $up->nama_ayah = $request->get('namaayah');
        $up->nama_ibu = $request->get('namaibu');
        $up->id_kelas = $request->get('id_kelas');


        if ($up->user) {
          $up->user->nama = $request->get('nama');
          $up->user->email = $request->get('email');
          $up->user->password = $pass;
          $up->user->tempatlahir = $request->get('tempatlahir');
          $up->user->jk = $request->get('jk');
          $up->user->tanggallahir = $request->get('tanggallahir');
          $up->user->telepon = $request->get('nomertelepon');
          $up->user->alamat = $request->get('alamat');
          $up->user->foto = $file_store_name;
          $up->user->id_agama = $request->get('id_agama');
          $up->user->save();
        }

        $up->save();
       return redirect()->back()->with('alert', 'Post Berhasil Diubah');

     }else{
       $up = Siswa::find($id);
       $up->nis = $request->get('nis');
       $up->nama_ayah = $request->get('namaayah');
       $up->nama_ibu = $request->get('namaibu');
       $up->id_kelas = $request->get('id_kelas');


       if ($up->user) {
         $up->user->nama = $request->get('nama');
         $up->user->email = $request->get('email');
         $up->user->password = $pass;
         $up->user->tempatlahir = $request->get('tempatlahir');
         $up->user->jk = $request->get('jk');
         $up->user->tanggallahir = $request->get('tanggallahir');
         $up->user->telepon = $request->get('nomertelepon');
         $up->user->alamat = $request->get('alamat');
         $up->user->foto = $file_store_name;
         $up->user->id_agama = $request->get('id_agama');
         $up->user->save();
       }

       $up->save();
      return redirect()->back()->with('alert', 'Post Berhasil Diubah');
      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = Siswa::find($id);
        $del->delete();
        if ($del->user) {
          $del->user->delete();
    }

        return redirect('siswa')->with('alert','Berhasil menghapus data');
    }
}
