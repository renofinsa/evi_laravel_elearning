<?php

namespace App\Http\Controllers;

use Auth;
use App\Soal;
use App\Quiz;
use App\JawabanTerpilih;
use Illuminate\Http\Request;

class SoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('core.quiz.soal');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->hasFile('foto')){
        $filenameWithExt = $request->file('foto')->getClientOriginalName();

        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

        $extension = $request->file('foto')->getClientOriginalExtension();
        $file_store_name = $filename.'_'.time().'.'.$extension;
        $path = $request->file('foto')->storeAs('public/upload_foto_soal', $file_store_name);
      }else{
        $file_store_name = 'noimage.jpg';
      }

      $id = Soal::create([
        'id_quiz' =>request('idquiz'),
        'gambar' => $file_store_name,
        'soal' => request('soal'),
        'jawaban_a' => request('jawaban1'),
        'jawaban_b' => request('jawaban2'),
        'jawaban_c' => request('jawaban3'),
        'jawaban_d' => request('jawaban4'),
        'jawaban_e' => request('jawaban5'),
        'jawabanbenar' => request('jawabanbenar')
      ]);



      return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $quiz = Quiz::find($id);
        $hasil = JawabanTerpilih::where('id_quiz','=',$id);
        // $soal = Soal::count()->where('id_quiz','=',$id);
        $soal = Soal::all();
        return view('core.mulaiquiz.soal', compact('quiz','soal','hasil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $up = Soal::find($id);
        $up->gambar = ('kosong');
        $up->soal = $request->get('soal');
        $up->jawaban_a = $request->get('jawaban1');
        $up->jawaban_b = $request->get('jawaban2');
        $up->jawaban_c = $request->get('jawaban3');
        $up->jawaban_d = $request->get('jawaban4');
        $up->jawaban_e = $request->get('jawaban5');
        $up->jawabanbenar = $request->get('jawabanbenar');
        $up->save();
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $del = Soal::find($id);
      $del->delete();
      return redirect()->back();
    }
}
