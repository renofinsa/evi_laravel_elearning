<?php

namespace App\Http\Controllers;
use App\Quiz;
use App\Soal;
use App\Jawaban;
use Illuminate\Http\Request;

class QuizSoalController extends Controller
{

  public function index(){
    $soal = Soal::all();
  }

  public function store(Request $request)
  {
      $id = Soal::create([
        'id_quiz' =>request('idquiz'),
        'gambar' => ('kosong'),
        'soal' => request('soal')

      ]);

      Jawaban::create([
        'id_soal' => $id->id,
        'jawaban' => request('jawaban1'),
        'status' => request('benar1')
      ]);

      Jawaban::create([
        'id_soal' => $id->id,
        'jawaban' => request('jawaban2'),
        'status' => request('benar2')
      ]);

      Jawaban::create([
        'id_soal' => $id->id,
        'jawaban' => request('jawaban3'),
        'status' => request('benar3')
      ]);

      Jawaban::create([
        'id_soal' => $id->id,
        'jawaban' => request('jawaban4'),
        'status' => request('benar4')
      ]);

      Jawaban::create([
        'id_soal' => $id->id,
        'jawaban' => request('jawaban5'),
        'status' => request('benar5')
      ]);

      return redirect()->back();
  }

  // public function destroy(Request $request, $id)
  // {
  //     //
  //     // $is = $request->get('id');
  //
  //     $soal = Soal::find($id);
  //     $soal->delete();
  //     if ($soal->jawaban) {
  //       $soal->jawaban->delete();
  //
  //     }
  //
  //
  //
  //     return redirect()->back();
  // }



}
