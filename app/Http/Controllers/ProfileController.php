<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use App\Siswa;
use App\User;
use App\Agama;
use App\Guru;
use App\Kelas;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (checkPermission(['siswa'])) {
          $kelas = Kelas::all();
          $agama = Agama::where('id','<>',Auth::user()->id_agama)->get();
          $siswa = Siswa::where('id_users','=',Auth::user()->id)->get();
          return view('core.profile.siswa', compact('siswa','agama','kelas'));
        }elseif (checkPermission(['admin'])) {
          $agama = Agama::where('id','<>',Auth::user()->id_agama)->get();
          $user = User::where('id','=',Auth::user()->id)->get();
          return view('core.profile.admin', compact('user','agama'));
        }elseif (checkPermission(['pengajar'])) {
          $agama = Agama::where('id','<>',Auth::user()->id_agama)->get();
          $guru = Guru::where('id_users','=',Auth::user()->id)->get();
          return view('core.profile.pengajar', compact('agama','guru'));
        }else {
          echo 'null';
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if (checkPermission(['siswa'])) {
        if($request->hasFile('foto')){
          $filenameWithExt = $request->file('foto')->getClientOriginalName();

          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

          $extension = $request->file('foto')->getClientOriginalExtension();
          $file_store_name = $filename.'_'.time().'.'.$extension;
          $path = $request->file('foto')->storeAs('public/upload_foto_siswa', $file_store_name);
        }else{
          $file_store_name = $request->get('foto_lama');
        }



        if($request->get('password') == Auth::user()->password){
          $pass = Auth::user()->password;
        }else{
          $pass = Hash::make($request->get('password'));
        }

        $up = Siswa::find($id);
        $up->nis = $request->get('nis');
        $up->nama_ayah = $request->get('namaayah');
        $up->nama_ibu = $request->get('namaibu');
        // $up->id_kelas = $request->get('id_kelas');


        if ($up->user) {
          $up->user->nama = $request->get('nama');
          $up->user->email = $request->get('email');
          $up->user->password = $pass;
          $up->user->tempatlahir = $request->get('tempatlahir');
          $up->user->jk = $request->get('jk');
          $up->user->tanggallahir = $request->get('tanggallahir');
          $up->user->telepon = $request->get('nomertelepon');
          $up->user->alamat = $request->get('alamat');
          $up->user->foto = $file_store_name;
          $up->user->id_agama = $request->get('id_agama');
          $up->user->save();

        }

        $up->save();
        return redirect()->back()->with('alert','Berhasil menyimpan');

      }elseif (checkPermission(['pengajar'])) {

        if($request->hasFile('foto')){
          $filenameWithExt = $request->file('foto')->getClientOriginalName();

          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

          $extension = $request->file('foto')->getClientOriginalExtension();
          $file_store_name = $filename.'_'.time().'.'.$extension;
          $path = $request->file('foto')->storeAs('public/upload_foto', $file_store_name);
        }else{
          $file_store_name = $request->get('foto_lama');
        }

        if($request->get('password') == Auth::user()->password){
          $pass = Auth::user()->password;
        }else{
          $pass = Hash::make($request->get('password'));
        }

        $up = Guru::find($id);
        $up->nip = $request->get('nip');


        if ($up->users) {
          $up->users->nama = $request->get('nama');
          $up->users->email = $request->get('email');
          $up->users->password = $pass;
          $up->users->tempatlahir = $request->get('tempatlahir');
          $up->users->jk = $request->get('jk');
          $up->users->tanggallahir = $request->get('tanggallahir');
          $up->users->telepon = $request->get('nomertelepon');
          $up->users->alamat = $request->get('alamat');
          $up->users->foto = $file_store_name;
          $up->users->id_agama = $request->get('id_agama');
          $up->users->save();
        }

        $up->save();
        return redirect()->back()->with('alert','Berhasil menyimpan');
      }elseif (checkPermission(['admin'])) {
        if($request->hasFile('foto')){
          $filenameWithExt = $request->file('foto')->getClientOriginalName();

          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

          $extension = $request->file('foto')->getClientOriginalExtension();
          $file_store_name = $filename.'_'.time().'.'.$extension;
          $path = $request->file('foto')->storeAs('public/upload_foto', $file_store_name);
        }else{
          $file_store_name = $request->get('foto_lama');
        }

        if($request->get('password') == Auth::user()->password){
          $pass = Auth::user()->password;
        }else{
          $pass = Hash::make($request->get('password'));
        }

          $up = User::find($id);
          $up->nama = $request->get('nama');
          $up->email = $request->get('email');
          $up->password = $pass;
          $up->tempatlahir = $request->get('tempatlahir');
          $up->jk = $request->get('jk');
          $up->tanggallahir = $request->get('tanggallahir');
          $up->telepon = $request->get('nomertelepon');
          $up->alamat = $request->get('alamat');
          $up->foto = $file_store_name;
          $up->id_agama = $request->get('id_agama');
          $up->save();


        $up->save();
        return redirect()->back()->with('alert','Berhasil menyimpan');

      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
