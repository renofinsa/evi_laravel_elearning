<?php

namespace App\Http\Controllers;
use App\Guru;
use App\Agama;
use App\User;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
class PengajarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = User::all();
      $guru = Guru::all();
      $agama = Agama::all();

      return view('core.pengajar.index', compact('agama','user','guru'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //handle upload file
        if($request->hasFile('foto')){
          $filenameWithExt = $request->file('foto')->getClientOriginalName();

          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

          $extension = $request->file('foto')->getClientOriginalExtension();
          $file_store_name = $filename.'_'.time().'.'.$extension;
          $path = $request->file('foto')->storeAs('public/upload_foto', $file_store_name);
        }else{
          $file_store_name = 'noimage.jpg';
        }



          $id = User::create([
             'nama' => request('nama'),
             'email' => request('email'),
             'password' => Hash::make($request->get('password')),
             'level' => ('3'),
             'tempatlahir' => request('tempatlahir'),
             'tanggallahir' => request('tanggallahir'),
             'jk' => request('jk'),
             'telepon' => request('nomertelepon'),
             'alamat' => request('alamat'),
             'foto' => $file_store_name,
             'id_agama' => request('id_agama')
         ]);

          Guru::create([
           'id_users' => $id->id,
           'nip' => request('nip')
         ]);

         return redirect()->back()->with('alert', 'Post Ditambahkan');




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->hasFile('foto')){
        $filenameWithExt = $request->file('foto')->getClientOriginalName();

        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

        $extension = $request->file('foto')->getClientOriginalExtension();
        $file_store_name = $filename.'_'.time().'.'.$extension;
        $path = $request->file('foto')->storeAs('public/upload_foto', $file_store_name);
      }else{
        $file_store_name = $request->get('foto_lama');
      }

      if($request->get('password') == $request->get('password_lama')){
        $pass = $request->get('password');
      }else{
        $pass = Hash::make($request->get('password'));
      }

      $cek = $request->get('email');
      $email_lama = $request->get('email_lama');


      if ($cek == $email_lama) {
        $up = Guru::find($id);
        $up->nip = $request->get('nip');


        if ($up->users) {
          $up->users->nama = $request->get('nama');
          $up->users->email = $request->get('email');
          $up->users->password = $pass;
          $up->users->tempatlahir = $request->get('tempatlahir');
          $up->users->jk = $request->get('jk');
          $up->users->tanggallahir = $request->get('tanggallahir');
          $up->users->telepon = $request->get('nomertelepon');
          $up->users->alamat = $request->get('alamat');
          $up->users->foto = $file_store_name;
          $up->users->id_agama = $request->get('id_agama');
          $up->users->save();
        }

        $up->save();
        return redirect()->back()->with('alert', 'Berhasil');
      }else{
        $up = Guru::find($id);
        $up->nip = $request->get('nip');


        if ($up->users) {
          $up->users->nama = $request->get('nama');
          $up->users->email = $request->get('email');
          $up->users->password = $pass;
          $up->users->tempatlahir = $request->get('tempatlahir');
          $up->users->jk = $request->get('jk');
          $up->users->tanggallahir = $request->get('tanggallahir');
          $up->users->telepon = $request->get('nomertelepon');
          $up->users->alamat = $request->get('alamat');
          $up->users->foto = $file_store_name;
          $up->users->id_agama = $request->get('id_agama');
          $up->users->save();
        }

        $up->save();
        return redirect()->back()->with('alert', 'Berhasil');
      }





    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $del = Guru::find($id);
      $del->delete();
      if ($del->users) {
        $del->users->delete();
      }


      return redirect('pengajar')->with('alert','Berhasil menghapus data');
    }
}
