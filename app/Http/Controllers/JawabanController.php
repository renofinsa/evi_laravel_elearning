<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Quiz;
use App\Soal;
use App\JawabanTerpilih;
class JawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('core.quiz.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $idquiz = $request->get('idquiz');
      $pilihan = $request->get('pilihan');
      $id_soal = $request->get('id');
      // $jumlah = $request->get('jumlah');
      $jawabanbenar = $request->get('jawabanbenar');
      // $soal = Soal::findOrFail($id_soal);
      // $soal = Soal::where('id','=', $id_soal)->first();

      // $ss = new $soal->jawabanbenar;
      // $pp = $soal;
      // $opsibenar = $soal->jawabanbenar;


      //
      $score=0;
			$benar=0;
			$salah=0;
			$kosong=0;
      $i = 0;
      $nomor=$id_soal[$i];
				//id nomor soal

        for ($i=0;$i<10;$i++)
        {
            //id nomor soal
            $nomor = $id_soal[$i];

            //jika user tidak memilih jawaban
            if (empty($pilihan[$nomor])){
              $kosong++;
            }else{
              //jawaban dari user
              $jawaban=$pilihan[$nomor];
              $jawabanbenararray=$jawabanbenar[$nomor];

              // dd($jawabanbenararray);

              if($jawaban == $jawabanbenararray){
                //jika jawaban cocok (benar)
                $benar++;
              }else{
                //jika salah
                $salah++;
              }

            }
            $score = $benar*10;
          }








      JawabanTerpilih::create([
        'id_users' => Auth::user()->id,
        'id_quiz' => $request->get('idquiz'),
        'nilai' => ($score),
        'benar' => ($benar),
        'salah' => ($salah),
        'kosong' => ($kosong)
      ]);
      return redirect('quizsiswa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


    }
}
