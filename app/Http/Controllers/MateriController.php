<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Materi;
use App\MataPelajaran;
use App\GuruMatpel;
use App\Kelas;
use App\User;
use Auth;


class MateriController extends Controller
{
    /**
     * Display aa listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }
    public function index()
    {
      if (checkPermission(['admin'])) {
        $gurumatpel = GuruMatpel::all();
        $kelas = Kelas::all();
        $matpel = MataPelajaran::all();
        $user = User::all();
        $materi = Materi::all();

        return view('core.materi.index', compact('materi', 'kelas', 'user', 'matpel','gurumatpel'));
      }elseif (checkPermission(['siswa'])) {
        $gurumatpel = GuruMatpel::all();
        // $kelas = Kelas::all();
        // $matpel = MataPelajaran::all();
        // $user = User::all();
        // $materi = Materi::all();
        $materi = Materi::leftJoin('guru_matpels','materis.id_relasi','=','guru_matpels.id')
                  ->leftJoin('siswas','guru_matpels.id_kelas','=','siswas.id_kelas')
                  ->where('siswas.id_users','=', Auth::user()->id)
                  ->where('guru_matpels.status','=','1')
                  ->get();


        return view('core.materi.siswa', compact('materi', 'kelas', 'user', 'matpel','gurumatpel'));
      }elseif (checkPermission(['pengajar'])) {
        $gurumatpel = GuruMatpel::where('id_guru', '=',Auth::user()->id )->get();
        $kelas = Kelas::all();
        $matpel = MataPelajaran::all();
        $user = User::all();
        $materi = Materi::whereIn('id_pembuat', [1,Auth::user()->id])->get();

        return view('core.materi.index', compact('materi', 'kelas', 'user', 'matpel','gurumatpel'));
      } else{
        echo "Null";
      }




    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //handle upload file
      if($request->hasFile('nama_file')){
        $filenameWithExt = $request->file('nama_file')->getClientOriginalName();

        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

        $extension = $request->file('nama_file')->getClientOriginalExtension();
        $file_store_name = $filename.'_'.time().'.'.$extension;
        $path = $request->file('nama_file')->storeAs('public/upload_file_materi', $file_store_name);
      }else{
        $file_store_name = 'no file';
      }

      Materi::create([
       'judul' => request('judul'),
       'id_relasi' => request('idrelasi'),
       'nama_file' => $file_store_name,
       'id_pembuat' => Auth::user()->id
     ]);

     return redirect()->back()->with('alert', 'Materi Ditambahkan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->hasFile('nama_file')){
        $filenameWithExt = $request->file('nama_file')->getClientOriginalName();

        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

        $extension = $request->file('nama_file')->getClientOriginalExtension();
        $file_store_name = $filename.'_'.time().'.'.$extension;
        $path = $request->file('nama_file')->storeAs('public/upload_file_materi', $file_store_name);
      }else{
        $file_store_name = $request->get('file_lama');
      }
      $materi = Materi::find($id);
      $materi->judul = $request->get('judul');
      $materi->id_relasi = $request->get('idrelasi');
      $materi->nama_file = $file_store_name;
      $materi->save();

      return redirect()->back()->with('alert', 'Materi Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $materi = Materi::find($id);
      $materi->delete();

      return redirect()->back()->with('alert','Materi Berhasil Dihapus');
    }
}
