<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use App\Soal;
use App\User;
use App\Kelas;
use App\Komen;
use App\GuruMatpel;
use App\Siswa;
use App\JawabanTerpilih;
use Auth;
class JawabanTerpilihController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }
    public function index()
    {
      if (Auth::user()->level == 2) {

        $quiz = Quiz::
                  // leftJoin('soals as soso','soso.id_quiz','=','cc.id')
                  // leftJoin('gurumatpel as cc','quizzes.id','=','cc.id')
                  leftJoin('guru_matpels as a','quizzes.id_relasi','=','a.id')
                  ->leftJoin('siswas as b','a.id_kelas','=','b.id_kelas')
                  ->where('b.id_users','=', Auth::user()->id)
                  ->where('quizzes.status','=','1')
                  ->get([
                    'quizzes.created_at',
                    'quizzes.id as idquiz',
                    'quizzes.judul as judul',
                    'quizzes.info as info'
                  ]);

        // $siswa = Siswa::where('id_users','=',auth::user()->id)->;

        $relasi = GuruMatpel::all();
        // $quiz = Quiz::where('id_relasi','=','2')->get();

          // $siswa = Siswa::where('id_users','=',Auth::user()->id);

          // $relasi = GuruMatpel::where('id_kelas','=','$siswa->id_kelas');
          // $hasil = JawabanTerpilih::where('id_users','=',Auth::user()->id);
          return view ('core.mulaiquiz.index', compact('quiz','relasi','siswa'));
      }else{
        $quiz = Quiz::all();
      }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $quiz = Quiz::find($id);
      $soal = Soal::find($id);
      $komen = Komen::find($id);
      $hasil = JawabanTerpilih::
                where('id_quiz','=',$id)->get();

      return view ('core.mulaiquiz.quiz', compact('quiz','soal','komen','hasil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
