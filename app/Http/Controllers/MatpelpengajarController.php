<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MataPelajaran;
use App\GuruMatpel;
use App\Guru;
use App\Kelas;
use App\User;
use App\Semester;
class MatpelpengajarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }
    public function index()
    {
        //

        $semester = Semester::all();
        $user = User::all();
        $guru = Guru::all();
        $kelas = Kelas::all();
        $gurumatpel = GuruMatpel::all();
        $matpel = MataPelajaran::all();
        return view('core.matpelpengajar.index', compact('matpel','user','guru','kelas','gurumatpel','semester'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          GuruMatpel::create([
            'id_guru' =>request('idguru'),
            'id_matpel' =>request('idmatpel'),
            'id_kelas' =>request('idkelas'),
            'id_semester' => request('idsemester'),
            'status' => ('1')
          ]);
          return redirect()->back()->with('success', 'Post Ditambahkan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $up = GuruMatpel::find($id);
      $up->id_guru = $request->get('idguru');
      $up->id_matpel = $request->get('idmatpel');
      $up->id_kelas = $request->get('idkelas');
      $up->id_semester = $request->get('idsemester');
      $up->status = $request->get('status');
      $up->save();

      return redirect()->back()->with('success', 'Post Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = GuruMatpel::find($id);
        $del->delete();
        return redirect()->back()->with('success', 'Post Dihapus');
    }
}
