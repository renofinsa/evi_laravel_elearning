<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JawabanTerpilih extends Model
{
    protected $fillable = [
      'id_users',
      'id_quiz',
      'nilai',
      'benar',
      'salah',
      'kosong'
    ];
    public function users(){
      return $this->belongsTo(User::class,'id_users');
    }
    public function quiz(){
      return $this->belongsToMany(Quiz::class,'id_quiz');
    }
}
