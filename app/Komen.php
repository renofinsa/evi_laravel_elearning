<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komen extends Model
{
    protected $table = 'komen';

    protected $fillable = [
      'id_quiz',
      'id_users',
      'pesan',
      'status'
    ];

    public function quiz(){
      return $this->belongsTo(Quiz::class,'id_quiz');
    }
    public function users(){
      return $this->belongsTo(User::class,'id_users');
    }
}
