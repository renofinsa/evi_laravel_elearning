<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class soal extends Model
{
    protected $fillable = [
      'id_quiz',
      'gambar',
      'soal',
      'jawaban_a',
      'jawaban_b',
      'jawaban_c',
      'jawaban_d',
      'jawaban_e',
      'jawabanbenar'
    ];

    public function quiz(){
      return $this->belongsTo(Quiz::class,'id_quiz');
    }
}
