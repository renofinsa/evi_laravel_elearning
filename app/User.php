<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Siswa;
use App\Guru;
use App\Berita;
use App\Materi;
class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama',
        'email',
        'password',
        'level',
        'tempatlahir',
        'tanggallahir',
        'jk',
        'alamat',
        'telepon',
        'foto',
        'id_agama'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

  //   public function admin(){
  //   return $this->hasOne('Admin','id_users');
  //   // return $this->hasOne('nama_model','id_relasi_dari_model_yang_dituju');
  // }
  //
    //   public function siswa(){
    //   return $this->hasOne(Siswa::class);
    // }



      public function agama(){
      return $this->belongsTo(Agama::class,'id_agama');
    }
      public function guru(){
      return $this->belongsTo(Guru::class,'id_users');
    }
      public function siswa(){
      return $this->belongsTo(Guru::class,'id_users');
    }
      public function berita(){
      return $this->hasMany(Guru::class,'id_pembuat');
    }
      public function materi(){
      return $this->hasMany(Materi::class, 'id_pembuat');
    }
      public function quiz(){
      return $this->hasMany(Quiz::class, 'id_pembuat');
    }
}
