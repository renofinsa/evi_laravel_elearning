<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Kelas;
use App\MataPelajaran;
use App\User;

class materi extends Model
{
    protected $table = 'materis';

    protected $fillable = [
      'judul',
      'id_relasi',
      'nama_file',
      'id_pembuat',
      'created_at'
    ];

    public function relasi(){
    return $this->belongsTo(GuruMatpel::class,'id_relasi');
    // return $this->hasOne('nama_model','id_relasi_dari_model_yang_dituju');
  }
    public function users(){
    return $this->belongsTo(User::class,'id_pembuat');
    // return $this->hasOne('nama_model','id_relasi_dari_model_yang_dituju');
  }

}
