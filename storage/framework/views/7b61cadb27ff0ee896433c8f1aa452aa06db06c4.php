<?php $__env->startSection('title', 'Manajemen Materi'); ?>

<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <?php if(\Session::has('alert')): ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p><?php echo e(\Session::get('alert')); ?></p>
      </div>
      <br />
      <?php endif; ?>
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Manajemen Materi</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Index</h3>

            <div class="card-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                  <button class="btn btn-success btn-sm" type="button" name="button" data-toggle="modal" data-target="#tambah">Tambah Materi</button>
              </div>
              <!-- MODAL TAMBAH -->
              <div id="tambah" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Tambah Materi</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="<?php echo e(route('materi.store')); ?>" method="post" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <div class="form-group has-feedback">
                          <label for="">Judul</label>
                          <input type="text" name="judul" value="" class="form-control<?php echo e($errors->has('judul') ? ' is-invalid' : ''); ?>" required>
                          <?php if($errors->has('judul')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('judul')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="kelas">Kelas & MataPelajaran</label>
                          <select class="form-control<?php echo e($errors->has('idrelasi') ? ' is-invalid' : ''); ?>" name="idrelasi" id="" required>
                          
                            <?php $__currentLoopData = $gurumatpel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($gm->id); ?>"><?php echo e($gm->kelas->kelas); ?> | <?php echo e($gm->matpel->matpel); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>

                                <?php if($errors->has('idrelasi')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('idrelasi')); ?></strong>
                                    </span>
                                <?php endif; ?>
                        </div>

                        <div class="form-group has-feedback">
                          <label for="">File</label>
                          <input type="file" name="nama_file" value="" class="form-control <?php echo e($errors->has('file') ? ' is-invalid' : ''); ?>" required>
                          <?php if($errors->has('file')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('file')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-info">Simpan</button>
                          <button type="button" name="button" class="btn btn-danger" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                    <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                  </div>

                </div>
              </div>
              <!-- MODAL TAMBAH -->
            </div>
          </div>
          <!-- /.card-header -->
          <div class="clearfix">

          </div>
          <div class="card-body table-responsive p-3">
            <table id="example1" class="table table-hover table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Keterangan</th>
                <th>Nama File</th>
                <th>Tanggal Posting</th>
                <th>Pembuat</th>
                <th>Aksi</th>
            </thead>
            <tbody>
              <?php $no = 0; ?>
              <?php $__currentLoopData = $materi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $m): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php $no++ ?>
              <tr>
                <td><?php echo e($no); ?></td>
                <td><?php echo e($m->judul); ?></td>
                <td><?php echo e($m->relasi->kelas->kelas); ?> | <?php echo e($m->relasi->matpel->matpel); ?></td>
                <td><?php echo e($m->nama_file); ?></td>
                <td><?php echo e(date('d F Y', strtotime($m->created_at))); ?></td>
                <td><?php echo e($m->users->nama); ?></td>
                <td>


                  <button class="btn btn-primary btn-sm" type="button" name="button" data-toggle="modal" data-target="#edit_<?php echo e($m->id); ?>">Ubah</button>
                  <button class="btn btn-danger btn-sm" type="button" name="button" data-toggle="modal" data-target="#delete_<?php echo e($m->id); ?>">Hapus</button>

                </td>
              </tr>

              <!-- MODAL EDIT -->
              <div id="edit_<?php echo e($m->id); ?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Ubah Materi</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="<?php echo e(route('materi.update', $m->id)); ?>" method="post" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group has-feedback">
                          <label for="">Judul</label>
                          <input type="text" name="judul" value="<?php echo e($m->judul); ?>" class="form-control<?php echo e($errors->has('idrelasi') ? ' is-invalid' : ''); ?>" required>
                          <?php if($errors->has('judul')): ?>
                              <span class="invalid-feedback">
                                  <strong><?php echo e($errors->first('judul')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="kelas">Pilih Kelas</label>
                          <select class="form-control<?php echo e($errors->has('idrelasi') ? ' is-invalid' : ''); ?>" name="idrelasi" id="" required>
                            <option value="<?php echo e($m->id_relasi); ?>"><?php echo e($m->relasi->kelas->kelas); ?> | <?php echo e($m->relasi->matpel->matpel); ?></option>
                            <?php $__currentLoopData = $gurumatpel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($gm->id); ?>"><?php echo e($gm->kelas->kelas); ?> | <?php echo e($gm->matpel->matpel); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>


                        </div>
                        <div class="form-group has-feedback">
                          <label for="">File</label>
                          <input type="hidden" value="<?php echo e($m->nama_file); ?>" name="file_lama">
                          <input type="file" name="nama_file" class="form-control<?php echo e($errors->has('nama_file') ? ' is-invalid' : ''); ?>">
                          <?php if($errors->has('nama_file')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('nama_file')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-success btn-sm">Simpan</button>
                          <button type="button" name="button" class="btn btn-warning btn-sm" data-dismiss=modal>Batal</button>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL EDIT -->
              <!-- MODAL DELETE -->
              <div id="delete_<?php echo e($m->id); ?>" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Hapus Materi</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="<?php echo e(route('materi.destroy', $m->id)); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="_method" value="DELETE">
                        <p>Yakin Anda ingin menghapus materi <?php echo e($m->judul); ?></p>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-danger btn-sm">Hapus</button>
                          <button type="button" name="button" class="btn btn-secondary btn-sm" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL DELETE -->
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Keterangan</th>
                <th>Nama File</th>
                <th>Tanggal Posting</th>
                <th>Pembuat</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
            </table>
          </div>
          <!-- /.card-body -->

        </div>
        <!-- /.card -->
      </div>
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>