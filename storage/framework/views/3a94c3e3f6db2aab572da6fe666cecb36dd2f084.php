<?php $__env->startSection('title', 'Quiz'); ?>

<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <?php if(\Session::has('alert')): ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p><?php echo e(\Session::get('alert')); ?></p>
      </div>
      <br />
      <?php endif; ?>
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Quiz</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Index</h3>

            <div class="card-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                  <?php if(checkPermission(['pengajar'])): ?>
                    <button class="btn btn-success btn-sm" type="button" name="button" data-toggle="modal" data-target="#tambah">Tambah Quiz</button>
                  <?php endif; ?>
              </div>
              <!-- MODAL TAMBAH -->
              <div id="tambah" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Tambah Quiz</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="<?php echo e(route('quiz.store')); ?>" method="post" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <div class="form-group has-feedback">
                          <label for="">Judul</label>
                          <input type="text" name="judul" value="" class="form-control<?php echo e($errors->has('judul') ? ' is-invalid' : ''); ?>" required>
                          <?php if($errors->has('judul')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('judul')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="kelas">Kelas & Mata Pelajaran</label>
                          <select class="form-control<?php echo e($errors->has('idrelasi') ? ' is-invalid' : ''); ?>" name="idrelasi" id="" required>
                            <?php $__currentLoopData = $gurumatpel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($gm->id); ?>"><?php echo e($gm->kelas->kelas); ?> | <?php echo e($gm->matpel->matpel); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>

                                <?php if($errors->has('idrelasi')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('idrelasi')); ?></strong>
                                    </span>
                                <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Info</label>
                          <textarea name="info" rows="5" class="form-control" placeholder="tulis informasi mengenai quiz disini ...." required></textarea>
                          <?php if($errors->has('file')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('file')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-info">Simpan</button>
                          <button type="button" name="button" class="btn btn-danger" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                    <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                  </div>

                </div>
              </div>
              <!-- MODAL TAMBAH -->
            </div>
          </div>
          <!-- /.card-header -->
          <div class="clearfix">

          </div>
          <div class="card-body table-responsive p-3">
            <table id="example1" class="table table-hover table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Keterangan</th>
                <th>Tanggal Posting</th>
                <th>Pembuat</th>
                <?php if(checkPermission(['pengajar'])): ?>
                  <th>Aksi</th>
                <?php endif; ?>
            </thead>
            <tbody>
              <?php $no = 0;
                $color = 'red';
              ?>
              <?php $__currentLoopData = $quiz; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $no++ ?>
                <tr>
                  <?php if($key->status == 0): ?>
                      <td style="background : red"><?php echo e($no); ?></td>
                  <?php elseif($key->status == 1): ?>
                    <td><?php echo e($no); ?></td>
                  <?php endif; ?>



                  <td><?php echo e($key->judul); ?></td>
                  <td><?php echo e($key->relasi->kelas->kelas); ?> | <?php echo e($key->relasi->matpel->matpel); ?></td>

                  <td><?php echo e(date('d F Y', strtotime($key->created_at))); ?></td>
                  <td><?php echo e($key->users->nama); ?></td>
                  <?php if(checkPermission(['pengajar'])): ?>
                    <td>
                      
                      <button class="btn btn-primary btn-sm" type="button" name="button" data-toggle="modal" data-target="#edit_<?php echo e($key->id); ?>">Ubah</button>
                      <button class="btn btn-danger btn-sm" type="button" name="button" data-toggle="modal" data-target="#delete_<?php echo e($key->id); ?>">Hapus</button>
                      <a class="btn btn-info btn-sm" href="quiz/<?php echo e($key->id); ?>">Buat Soal</a>
                      <a class="btn btn-default btn-sm" href="quizsiswa/<?php echo e($key->id); ?>">Forum</a>
                      <a style="color : #fff" class="btn btn-warning btn-sm" href="hasil/<?php echo e($key->id); ?>">Nilai</a>
                      
                      
                      
                    </td>
                  <?php endif; ?>

                </tr>



              <!-- MODAL EDIT -->
              <div id="edit_<?php echo e($key->id); ?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Ubah Data</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                      <form class="" action="<?php echo e(route('quiz.update', $key->id)); ?>" method="post" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group has-feedback">
                          <label for="">Judul</label>
                          <input type="text" name="judul" value="<?php echo e($key->judul); ?>" class="form-control<?php echo e($errors->has('judul') ? ' is-invalid' : ''); ?>" required>
                          <?php if($errors->has('judul')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('judul')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="Relasi">Kelas & Mata Pelajaran</label>
                          <select class="form-control<?php echo e($errors->has('idrelasi') ? ' is-invalid' : ''); ?>" name="idrelasi" id="" required>
                            <option value="<?php echo e($key->id_relasi); ?>"><?php echo e($key->relasi->kelas->kelas); ?> | <?php echo e($key->relasi->matpel->matpel); ?> </option>
                            <?php $__currentLoopData = $gurumatpel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($gm->id); ?>"><?php echo e($gm->kelas->kelas); ?> | <?php echo e($gm->matpel->matpel); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>

                                <?php if($errors->has('idrelasi')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('idrelasi')); ?></strong>
                                    </span>
                                <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Info</label>
                          <textarea name="info" rows="5" class="form-control" placeholder="tulis informasi mengenai quiz disini ...."><?php echo e($key->info); ?></textarea>
                          <?php if($errors->has('file')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('file')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Jenis Kelamin</label>
                          <div class="row">
                            <div class="col-md-6">
                              <input type="radio" name="status" value="1"
                              <?php if($key->status =='1' ): ?>
                                checked
                              <?php endif; ?>
                                class="<?php echo e($errors->has('status') ? ' is-invalid' : ''); ?>"> Aktif<br>

                            </div>
                            <div class="col-md-6">
                                <input type="radio" name="status" value="0"
                                 <?php if($key->status =='0' ): ?>
                                  checked
                                <?php endif; ?>
                                class="<?php echo e($errors->has('status') ? ' is-invalid' : ''); ?>"> Tidak Aktif<br>

                            </div>
                          </div>
                          <?php if($errors->has('jk')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('jk')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-success btn-sm">Simpan</button>
                          <button type="button" name="button" class="btn btn-warning btn-sm" data-dismiss=modal>Batal</button>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL EDIT -->
              <!-- MODAL DELETE -->
              <div id="delete_<?php echo e($key->id); ?>" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Hapus Materi</h4>
                    </div>
                    <div class="modal-body">
                      <form class="" action="<?php echo e(route('quiz.destroy', $key->id)); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="_method" value="DELETE">
                        <p>Yakin Anda ingin menghapus materi <?php echo e($key->judul); ?></p>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-danger btn-sm">Hapus</button>
                          <button type="button" name="button" class="btn btn-secondary btn-sm" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL DELETE -->
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Keterangan</th>
                <th>Tanggal Posting</th>
                <th>Pembuat</th>
                  <?php if(checkPermission(['pengajar'])): ?>
                    <th>Aksi</th>
                  <?php endif; ?>

              </tr>
            </tfoot>
            </table>

          </div>
          <!-- /.card-body -->

        </div>

        <!-- /.card -->
      </div>
      <div class="row" style="margin-left : 5%">
        <div class="col-12">
          <div class="form-group">
            <h3>Pesan : </h3>
            <p>Jika soal quiz kurang dari 10 maka Quiz tidak dapat aktif</p>
          </div>
        </div>
      </div>
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>