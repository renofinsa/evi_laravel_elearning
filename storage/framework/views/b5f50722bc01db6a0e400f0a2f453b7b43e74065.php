<nav class="main-header navbar navbar-expand  navbar-light border-bottom" style="background-color : #46a8af">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#"><i style="color : #fff" class="fa fa-bars"></i></a>
    </li>
  </ul>

  <!-- SEARCH FORM -->
  <form class="form-inline ml-3">
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fa fa-search"></i>
        </button>
      </div>
    </div>
  </form>

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <!-- Notifications Dropdown Menu -->
    <li class="nav-item dropdown">
      <a class="nav-link" data-toggle="dropdown" href="#">
        <i style="color : #fff" class="fa fa-th-large"></i>
      </a>
      <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        <div class="dropdown-divider"></div>
        <a href="#" class="dropdown-item">
           Hello, <?php echo e(Auth::user()->nama); ?>

        </a>
        <div class="dropdown-divider"></div>
        <a href="/pengaturan" class="dropdown-item">
          <i class="fa fa-user mr-2"></i> Pengaturan
        </a>
        <div class="dropdown-divider"></div>
        <a href="<?php echo e(route('logout')); ?>" class="dropdown-item" onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
          <i class="fa fa-sign-out mr-2"></i> Logout
        </a>
        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
            <?php echo csrf_field(); ?>
        </form>
      </div>
    </li>
  </ul>
</nav>
