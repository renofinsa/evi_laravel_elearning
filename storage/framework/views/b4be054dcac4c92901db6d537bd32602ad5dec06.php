<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-image: url('../../dist/img/bg_inside.jpg'">
  <!-- Brand Logo -->
  <a href="#" class="brand-link">
    <img src="../../dist/img/logo.png" alt="Logo SMAN 25" class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">E-learning</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <?php if(checkPermission(['admin'])): ?>
          <img src="../storage/app/public/upload_foto/<?php echo e(auth::user()->foto); ?>" class="img-circle elevation-2" alt="User Image">
        <?php endif; ?>
        <?php if(checkPermission(['siswa'])): ?>
          <img src="../storage/app/public/upload_foto_siswa/<?php echo e(auth::user()->foto); ?>" class="img-circle elevation-2" alt="User Image">
        <?php endif; ?>
        <?php if(checkPermission(['pengajar'])): ?>
          <img src="../storage/app/public/upload_foto/<?php echo e(auth::user()->foto); ?>" class="img-circle elevation-2" alt="User Image">
        <?php endif; ?>

      </div>
      <div class="info">
        <a href="#" class="d-block"> <?php echo e(Auth::user()->nama); ?></a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
      <?php if(checkPermission(['pengajar'])): ?>
        <li class="nav-header">PENGAJAR</li>
        <li class="nav-item has-treeview menu-open">
          <a href="/home" class="nav-link active">
            <i class="nav-icon fa fa-dashboard"></i>
            <p>
              Home
            </p>
          </a>
        </li>
        
        <li class="nav-item has-treeview">
          <a href="/quiz" class="nav-link">
            <i class="nav-icon fa fa-edit"></i>
            <p>
              Manajemen Tugas/Quiz
            </p>
          </a>
        </li>
        <?php endif; ?>
        <?php if(checkPermission(['siswa'])): ?>
        <li class="nav-header">SISWA</li>
        <li class="nav-item has-treeview menu-open">
          <a href="/home" class="nav-link active">
            <i class="nav-icon fa fa-dashboard"></i>
            <p>
              Home
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="/quizsiswa" class="nav-link">
            <i class="nav-icon fa fa-pie-chart"></i>
            <p>
              Tugas/Quiz
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="/materi" class="nav-link">
            <i class="nav-icon fa fa-tree"></i>
            <p>
              Materi
            </p>
          </a>
        </li>
        <?php endif; ?>
        <?php if(checkPermission(['admin'])): ?>
        <li class="nav-header">ADMINISTRATOR</li>
        <li class="nav-item has-treeview menu-open">
          <a href="/home" class="nav-link active">
            <i class="nav-icon fa fa-dashboard"></i>
            <p>
              Home
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="pages/widgets.htm" class="nav-link">
            <i class="nav-icon fa fa-th"></i>
            <p>
              Manajemen Akun
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/pengajar" class="nav-link">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Manajemen Pengajar</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/siswa" class="nav-link">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Manajemen Siswa</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview">
          <a href="/kelas" class="nav-link">
            <i class="nav-icon fa fa-pie-chart"></i>
            <p>
              Manajemen Kelas
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="/semester" class="nav-link">
            <i class="nav-icon fa fa-tree"></i>
            <p>
              Manajemen Semester
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="/matapelajaran" class="nav-link">
            <i class="nav-icon fa fa-tree"></i>
            <p>
              Mata Pelajaran
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="/matpelpengajar" class="nav-link">
            <i class="nav-icon fa fa-pie-chart"></i>
            <p>
              Mata Pelajaran & Pengajar
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="/quiz" class="nav-link">
            <i class="nav-icon fa fa-edit"></i>
            <p>
              Manajemen Tugas/Quiz
            </p>
          </a>
        </li>

        <?php endif; ?>
        <?php if(checkPermission(['admin', 'pengajar'])): ?>
          <li class="nav-item has-treeview">
            <a href="/materi" class="nav-link">
              <i class="nav-icon fa fa-table"></i>
              <p>
                Materi
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="/berita" class="nav-link">
              <i class="nav-icon fa fa-table"></i>
              <p>
                Berita
              </p>
            </a>
          </li>
        <?php endif; ?>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
