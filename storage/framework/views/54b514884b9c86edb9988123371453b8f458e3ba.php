<?php $__env->startSection('title', 'Quiz'); ?>

<?php $__env->startSection('content'); ?>
<div class="content-wrapper" >
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <?php if(\Session::has('alert')): ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p><?php echo e(\Session::get('alert')); ?></p>
      </div>
      <br />
      <?php endif; ?>
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?php echo e($quiz->judul); ?></h1>
        </div>

      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      
      <div style="margin-left : 20%" class="col-md-8">


        <?php $no = 0; ?>
        <?php $__currentLoopData = $quiz->soal()->inRandomOrder()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $soal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <?php $no++ ?>
          <form class="" action="<?php echo e(route('jawaban.store')); ?>" method="post">
            <?php echo csrf_field(); ?>
            <input type="hidden" name="id[]" value="<?php echo e($soal->id); ?>">
            
            <input type="hidden" name="idquiz" value="<?php echo e($quiz->id); ?>">
            <input type="hidden" name="jawabanbenar[<?php echo e($soal->id); ?>]" value="<?php echo e($soal->jawabanbenar); ?>">

          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-md-1">
                  <h3><?php echo e($no); ?> </h3>
                </div>
                <div class="col-md-11">

                      <div class="row">

                          <?php if($soal->gambar == 'noimage.jpg'): ?>

                          <?php elseif($soal->gambar): ?>
                            <div class="col-md-4">
                              <img src="../../../storage/app/public/upload_foto_soal/<?php echo e($soal->gambar); ?>" width="200px" style="margin-right : 10px">
                            </div>
                          <?php endif; ?>



                        <div class="col-md-8">
                          <h3 class="card-title"><?php echo e($soal->soal); ?> </h3>
                        </div>
                      </div>


                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="card-body table-responsive p-3">
              <div class="form-group has-feedback">
                <input type="radio" name="pilihan[<?php echo e($soal->id); ?>]" value="a" style="margin-right: 10px"><label for="">A</label>
                <input type="text" name="jawaban1" class="form-control" value="<?php echo e($soal->jawaban_a); ?>" readonly>
              </div>

              <div class="form-group has-feedback">
                <input type="radio" name="pilihan[<?php echo e($soal->id); ?>]" value="b" style="margin-right: 10px"><label for="">B</label>
                <input type="text" name="jawaban2" class="form-control" value="<?php echo e($soal->jawaban_b); ?>" readonly>
              </div>

              <div class="form-group has-feedback">
                <input type="radio" name="pilihan[<?php echo e($soal->id); ?>]" value="c" style="margin-right: 10px"><label for="">C</label>
                <input type="text" name="jawaban3" class="form-control" value="<?php echo e($soal->jawaban_c); ?>" readonly>
              </div>

              <div class="form-group has-feedback">
                <input type="radio" name="pilihan[<?php echo e($soal->id); ?>]" value="d" style="margin-right: 10px"><label for="">D</label>
                <input type="text" name="jawaban4" class="form-control" value="<?php echo e($soal->jawaban_d); ?>" readonly>
              </div>

              <div class="form-group has-feedback">
                <input type="radio" name="pilihan[<?php echo e($soal->id); ?>]" value="e" style="margin-right: 10px"><label for="">E</label>
                <input type="text" name="jawaban5" class="form-control" value="<?php echo e($soal->jawaban_e); ?>" readonly>
              </div>
            </div>
          </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <div class="form-group">
          <input class="btn btn-success form-control" type="submit" name="submit" value="Jawab" onclick="return confirm('Apakah Anda yakin dengan jawaban Anda?')">
        </div>

        </form>

      </div>





    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>