<?php $__env->startSection('content'); ?>
<div class="login-box">

  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Welcome SMAN 25 Jakarta</p>
      <div class="login-logo">
        <img src="../dist/img/logo.png"  alt="User Image">
      </div>

                  <form method="POST" action="<?php echo e(route('login')); ?>" aria-label="<?php echo e(__('Login')); ?>">
                    <?php echo csrf_field(); ?>
                    <div class="form-group has-feedback">
                      <span class="fa fa-envelope form-control-feedback"></span>
                      <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" placeholder="Email" name="email" value="<?php echo e(old('email')); ?>" required autofocus>
                      <?php if($errors->has('email')): ?>
                          <span class="invalid-feedback" role="alert">
                              <strong><?php echo e($errors->first('email')); ?></strong>
                          </span>
                      <?php endif; ?>
                    </div>
                    <div class="form-group has-feedback">
                      <span class="fa fa-lock form-control-feedback"></span>
                      <input id="password" type="password" placeholder="Password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" required>
                      <?php if($errors->has('password')): ?>
                          <span class="invalid-feedback" role="alert">
                              <strong><?php echo e($errors->first('password')); ?></strong>
                          </span>
                      <?php endif; ?>
                    </div>
                    <div class="row">
                      
                      <!-- /.col -->
                      <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                      </div>
                      <!-- /.col -->
                    </div>
                  </form>
                  
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>