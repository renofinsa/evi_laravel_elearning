<?php $__env->startSection('title', 'Manajemen Pengajar'); ?>

<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <?php if(\Session::has('alert')): ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p><?php echo e(\Session::get('alert')); ?></p>
      </div>
      <br />
      <?php endif; ?>
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Manajemen Pengajar</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Index</h3>

            <div class="card-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                  <button class="btn btn-success btn-sm" type="button" name="button" data-toggle="modal" data-target="#tambah">Tambah Pengajar</button>
              </div>
              <!-- MODAL TAMBAH -->
              <div id="tambah" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Tambah Pengajar</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form class="" action="<?php echo e(route('pengajar.store')); ?>" method="post" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>

                        <div class="form-group has-feedback">
                          <label for="">Foto (Jpg,Png | Max 2000kb)</label>
                          <input required type="file" name="foto" value="lorem" class="form-control<?php echo e($errors->has('foto') ? ' is-invalid' : ''); ?>"
                          placeholder="Joko Anwar">
                          <?php if($errors->has('foto')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('foto')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nama Lengkap</label>
                          <input required style="text-transform : capitalize" type="text" name="nama" value="" class="form-control<?php echo e($errors->has('nama') ? ' is-invalid' : ''); ?>" required
                          placeholder="Joko Anwar">
                          <?php if($errors->has('nama')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('nama')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">NIP (Nomer Induk Pengajar)</label>
                          <input required type="number" name="nip" value="" class="form-control<?php echo e($errors->has('nis') ? ' is-invalid' : ''); ?>" required
                          placeholder="102030405060">
                          <?php if($errors->has('nis')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('nis')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label required for="">Email</label>
                          <input type="email" name="email" value="" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" required
                          placeholder="email@domain.com">
                          <?php if($errors->has('email')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('email')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Kata Sandi</label>
                          <input type="text" name="kode" value="" class="form-control" required placeholder="masukan kata sandi kamu disini">
                          <?php if($errors->has('kode')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('kode')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="agama">Agama</label>
                          <select class="form-control" name="id_agama" id="" required>
                            <?php $__currentLoopData = $agama; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $aa): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($aa->id); ?>"><?php echo e($aa->agama); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>

                                <?php if($errors->has('id_agama')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('id_agama')); ?></strong>
                                    </span>
                                <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Tempat Lahir</label>
                          <input id="tempatlahir" type="text" class="form-control<?php echo e($errors->has('tempatlahir') ? ' is-invalid' : ''); ?>" name="tempatlahir" required placeholder="Cth : Jakarta">
                          <?php if($errors->has('tempatlahir')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('tempatlahir')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Tanggal Lahir</label>
                          <input id="tanggallahir" type="date" class="form-control<?php echo e($errors->has('tanggallahir') ? ' is-invalid' : ''); ?>" name="tanggallahir" required >
                          <?php if($errors->has('tanggallahir')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('tanggallahir')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Jenis Kelamin</label>
                          <div class="row">
                            <div class="col-md-6">
                              <input type="radio" name="jk" value="1" checked class="<?php echo e($errors->has('gender') ? ' is-invalid' : ''); ?>"> Pria<br>
                            </div>
                            <div class="col-md-6">
                                <input type="radio" name="jk" value="2" class="<?php echo e($errors->has('gender') ? ' is-invalid' : ''); ?>"> Wanita<br>
                            </div>
                          </div>
                          <?php if($errors->has('jk')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('jk')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nomer Telepon</label>
                          <input type="number" name="nomertelepon"  class="form-control<?php echo e($errors->has('nomertelepon') ? ' is-invalid' : ''); ?>" required placeholder="+62822XXXXX">
                          <?php if($errors->has('nomertelepon')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('nomertelepon')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Alamat</label>
                          <textarea name="alamat" class="form-control<?php echo e($errors->has('alamat') ? ' is-invalid' : ''); ?>" rows="5" cols="80" placeholder="masukan alamat kamu disini" required></textarea>
                          <?php if($errors->has('alamat')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('alamat')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <button type="submit" name="button" class="btn btn-info">Simpan</button>
                          <button type="button" name="button" class="btn btn-danger" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                    <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                  </div>

                </div>
              </div>
              <!-- MODAL TAMBAH -->
            </div>
          </div>
          <!-- /.card-header -->
          <div class="clearfix">

          </div>
          <div class="card-body table-responsive p-3">
            <table id="example1" class="table table-hover table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>NIP</th>
                <th>Nama Lengkap</th>
                <th>Jenis Kelamin</th>
                <th>Nomer Telepon</th>
                <th>Foto Profil</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 0; ?>
              <?php $__currentLoopData = $guru; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php $no++ ?>

              <tr>
                <td><?php echo e($no); ?></td>
                <td><?php echo e($a->nip); ?></td>
                <td><?php echo e($a->users->nama); ?></td>
                <?php if($a->users->jk =='1'): ?>

                    <td>Pria</td>
                  <?php else: ?>
                    <td>Wanita</td>

                <?php endif; ?>
                <td><?php echo e($a->users->telepon); ?></td>
                <td><img src="storage/app/public/upload_foto/<?php echo e($a->users->foto); ?>" width="100px"></td>
                <td>
                  <button class="btn btn-primary btn-sm" type="button" name="button" data-toggle="modal" data-target="#edit_<?php echo e($a->id); ?>">Ubah</button>
                  <button class="btn btn-danger btn-sm" type="button" name="button" data-toggle="modal" data-target="#delete_<?php echo e($a->id); ?>">Hapus</button>
                  <button class="btn btn-info btn-sm" type="button" name="button" data-toggle="modal" data-target="#lihat_<?php echo e($a->id); ?>">Lihat</button>
                </td>
              </tr>
              <!-- MODAL LIHAT -->
              <div id="lihat_<?php echo e($a->id); ?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">NIP : <?php echo e($a->nip); ?></h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <center><img src="storage/app/public/upload_foto/<?php echo e($a->users->foto); ?>" width="150px"></center>
                          </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                              <h5>Nama : <?php echo e($a->users->nama); ?></h5>
                            </div>
                            <div class="form-group">
                              <h5>Jenis Kelamin :
                                <?php if($a->users->jk =='1'): ?>

                                    Pria
                                  <?php else: ?>
                                    Wanita

                                <?php endif; ?>

                              </h5>
                            </div>
                            <div class="form-group">
                              <h5>Agama : <?php echo e($a->users->agama->agama); ?></h5>
                            </div>
                        </div>
                      </div>
                      <hr>
                      <h5><b>Tempat, Tanggal Lahir :</b> <br><?php echo e($a->users->tempatlahir); ?>, <?php echo e($a->users->tanggallahir); ?></h5>
                      <h5><b>Nomer Telepon :</b> <br><?php echo e($a->users->telepon); ?></h5>
                      <h5><b>Email : </b></br><?php echo e($a->users->email); ?></h5>
                      <h5><b>Alamat : </b></br><?php echo e($a->users->alamat); ?></h5>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL LIHAT -->
              <!-- MODAL EDIT -->
              <div id="edit_<?php echo e($a->id); ?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Ubah Data Pengajar</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                      <form class="" action="<?php echo e(route('pengajar.update', $a->id)); ?>" method="post" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group">
                          <center><img src="storage/app/public/upload_foto/<?php echo e($a->users->foto); ?>" width="100px"></center>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Foto (Jpg,Png | Max 2000kb)</label>
                          <input type="hidden" value="<?php echo e($a->users->foto); ?>" name="foto_lama">
                          <input type="file" name="foto" class="form-control<?php echo e($errors->has('foto') ? ' is-invalid' : ''); ?>"
                          placeholder="Joko Anwar">
                          <?php if($errors->has('foto')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('foto')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nama Lengkap</label>
                          <input style="text-transform : capitalize" type="text" name="nama" value="<?php echo e($a->users->nama); ?>" class="form-control<?php echo e($errors->has('nama') ? ' is-invalid' : ''); ?>" required
                          placeholder="Joko Anwar">
                          <?php if($errors->has('nama')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('nama')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">NIP (Nomer Induk Pengajar)</label>
                          <input type="number" name="nip" value="<?php echo e($a->nip); ?>" class="form-control<?php echo e($errors->has('nis') ? ' is-invalid' : ''); ?>" required
                          placeholder="102030405060">
                          <?php if($errors->has('nis')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('nis')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Email</label>
                          <input type="email" name="email" value="<?php echo e($a->users->email); ?>" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" required
                          placeholder="email@domain.com">
                          <?php if($errors->has('email')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('email')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Kata Sandi</label>
                          <input type="password" name="password" value="<?php echo e($a->users->password); ?>" class="form-control" required placeholder="masukan kata sandi kamu disini">
                          <input type="hidden" name="password_lama" value="<?php echo e($a->users->password); ?>" class="form-control" required placeholder="masukan kata sandi kamu disini">

                        </div>
                        <div class="form-group has-feedback">
                          <label for="agama">Agama</label>
                          <select class="form-control" name="id_agama" id="" required>
                            <option  value="<?php echo e($a->users->agama->id); ?>"><?php echo e($a->users->agama->agama); ?></option>
                            <?php $__currentLoopData = $agama; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agamas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($agamas->id); ?>"><?php echo e($agamas->agama); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>

                                <?php if($errors->has('id_agama')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('id_agama')); ?></strong>
                                    </span>
                                <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Tempat Lahir</label>
                          <input value="<?php echo e($a->users->tempatlahir); ?>" id="tempatlahir" type="text" class="form-control<?php echo e($errors->has('tempatlahir') ? ' is-invalid' : ''); ?>" name="tempatlahir" required placeholder="Cth : Jakarta">
                          <?php if($errors->has('tempatlahir')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('tempatlahir')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Tanggal Lahir</label>
                          <input value="<?php echo e($a->users->tanggallahir); ?>" id="tanggallahir" type="date" class="form-control<?php echo e($errors->has('tanggallahir') ? ' is-invalid' : ''); ?>" name="tanggallahir" required >
                          <?php if($errors->has('tanggallahir')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('tanggallahir')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Jenis Kelamin</label>
                          <div class="row">
                            <div class="col-md-6">
                              <input type="radio" name="jk" value="1"
                              <?php if($a->users->jk =='1' ): ?>
                                checked
                              <?php endif; ?>
                                class="<?php echo e($errors->has('jk') ? ' is-invalid' : ''); ?>"> Pria<br>

                            </div>
                            <div class="col-md-6">
                                <input type="radio" name="jk" value="2"
                                 <?php if($a->users->jk =='2' ): ?>
                                  checked
                                <?php endif; ?>
                                class="<?php echo e($errors->has('jk') ? ' is-invalid' : ''); ?>"> Wanita<br>

                            </div>
                          </div>
                          <?php if($errors->has('jk')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('jk')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Nomer Telepon</label>
                          <input value="<?php echo e($a->users->telepon); ?>" type="number" name="nomertelepon"  class="form-control<?php echo e($errors->has('nomertelepon') ? ' is-invalid' : ''); ?>" required placeholder="+62822XXXXX">
                          <?php if($errors->has('nomertelepon')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('nomertelepon')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <label for="">Alamat</label>
                          <textarea  name="alamat" class="form-control<?php echo e($errors->has('alamat') ? ' is-invalid' : ''); ?>" rows="5" cols="80" placeholder="masukan alamat kamu disini" required><?php echo e($a->users->alamat); ?></textarea>
                          <?php if($errors->has('alamat')): ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($errors->first('alamat')); ?></strong>
                              </span>
                          <?php endif; ?>
                        </div>
                        <div class="form-group has-feedback">
                          <button type="submit" name="button" class="btn btn-info">Simpan</button>
                          <button type="button" name="button" class="btn btn-danger" data-dismiss=modal>Batal</button>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL EDIT -->
              <!-- MODAL DELETE -->
              <div id="delete_<?php echo e($a->id); ?>" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Hapus Data Pengajar</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                      <form class="" action="<?php echo e(route('pengajar.destroy', $a->id)); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="_method" value="DELETE">
                        <p>Yakin Anda ingin menghapus data <b><?php echo e($a->users->nama); ?></b> </p>
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-danger btn-sm">Hapus</button>
                          <button type="button" name="button" class="btn btn-secondary btn-sm" data-dismiss=modal>Batal</button>
                        </div>

                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- MODAL DELETE -->



              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>NIP</th>
                <th>Nama Lengkap</th>
                <th>Jenis Kelamin</th>
                <th>Nomer Telepon</th>
                <th>Foto Profil</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
            </table>
          </div>
          <!-- /.card-body -->

        </div>
        <!-- /.card -->
      </div>
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>