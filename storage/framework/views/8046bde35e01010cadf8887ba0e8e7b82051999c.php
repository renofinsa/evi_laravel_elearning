<?php $__env->startSection('title', 'Manajemen Materi'); ?>

<?php $__env->startSection('content'); ?>
<div class="content-wrapper" style="background-image: url('dist/img/bg_out.jpg'">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <?php if(\Session::has('alert')): ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p><?php echo e(\Session::get('alert')); ?></p>
      </div>
      <br />
      <?php endif; ?>
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Materi</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <?php $__currentLoopData = $materi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <div class="col-3">
        <div class="card">
          <div class="card-header">


                <h5><?php echo e($key->judul); ?></h5>
                <hr>
                
                <center><a href="storage/app/public/upload_file_materi/<?php echo e($key->nama_file); ?>" target="_blank" download class="btn btn-info"><i class="fa fa-download"></i> Download</a></center>



          </div>
          <!-- /.card-header -->
          <div class="clearfix">

          </div>

          <!-- /.card-body -->

        </div>
        <!-- /.card -->
      </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>