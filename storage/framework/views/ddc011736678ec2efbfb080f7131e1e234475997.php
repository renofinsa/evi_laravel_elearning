<?php $__env->startSection('content'); ?>
<div class="register-box">
<div class="register-logo">
  <img src="dist/img/personel.png" class="img-circle elevation-2" alt="User Image">
</div>
                <div class="card">
                  <div class="card-body register-card-body">
                    <p class="login-box-msg"><?php echo e(__('Register a new membership')); ?></p>

                    <form method="POST" action="<?php echo e(route('register')); ?>" aria-label="<?php echo e(__('Register')); ?>">
                        <?php echo csrf_field(); ?>
                      <div class="form-group has-feedback">
                        <span class="fa fa-user form-control-feedback"></span>
                        <input id="name" type="text" placeholder="Full name" class="form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" name="name" value="<?php echo e(old('name')); ?>" required autofocus>
                        <?php if($errors->has('name')): ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($errors->first('name')); ?></strong>
                            </span>
                        <?php endif; ?>
                      </div>
                      <div class="form-group has-feedback">
                        <span class="fa fa-envelope form-control-feedback"></span>
                        <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required placeholder="Email">
                        <?php if($errors->has('email')): ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($errors->first('email')); ?></strong>
                            </span>
                        <?php endif; ?>
                      </div>
                      <div class="form-group has-feedback">
                          <span class="fa fa-users form-control-feedback"></span>

                              <select id="level" class="form-control<?php echo e($errors->has('level') ? ' is-invalid' : ''); ?>" name="level">
                                <option value="0"></option>
                                <option value="1">Admin</option>
                                <option value="2">Siswa</option>
                                <option value="3">Dosen</option>
                              </select>

                              <?php if($errors->has('level')): ?>
                                  <span class="invalid-feedback">
                                      <strong><?php echo e($errors->first('level')); ?></strong>
                                  </span>
                              <?php endif; ?>
                      </div>
                      <div class="form-group has-feedback">
                        <span class="fa fa-lock form-control-feedback"></span>
                        <input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" required placeholder="Password">
                        <?php if($errors->has('password')): ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($errors->first('password')); ?></strong>
                            </span>
                        <?php endif; ?>
                      </div>
                      <div class="form-group has-feedback">
                        <span class="fa fa-lock form-control-feedback"></span>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Retype Password" required>
                      </div>
                      <div class="row">
                        <div class="col-8">
                          <div class="checkbox icheck">
                            <label>
                              <input type="checkbox"> I agree to the <a href="#">terms</a>
                            </label>
                          </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-4">
                          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                        </div>
                        <!-- /.col -->
                      </div>
                    </form>

                    <a href="<?php echo e(route('login')); ?>" class="text-center">I already have a membership</a>
                  </div>
                  <!-- /.form-box -->
                </div><!-- /.card -->
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>