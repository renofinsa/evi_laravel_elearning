<?php $__env->startSection('title', 'Quiz'); ?>

<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <?php if(\Session::has('alert')): ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p><?php echo e(\Session::get('alert')); ?></p>
      </div>
      <br />
      <?php endif; ?>
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Tugas / Quiz</h1>
        </div>

      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <?php $__currentLoopData = $quiz; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $qz): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-4">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><?php echo e($qz->judul); ?></h3>
              <p style="text-align : right"><?php echo e($qz->created_at->diffForHumans()); ?></p>
            </div>
            <div class="clearfix"></div>
            <div class="card-body table-responsive p-3">
              <h5>Info :</h5>
              <p><?php echo e($qz->info); ?></p>
            </div>
            <a href="quizsiswa/<?php echo e($qz->idquiz); ?>" class="btn btn-success form-control">Lihat</a>
          </div>
        </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>