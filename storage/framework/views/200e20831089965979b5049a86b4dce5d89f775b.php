<?php $__env->startSection('title', Auth::user()->nama); ?>

<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <?php if(\Session::has('alert')): ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p><?php echo e(\Session::get('alert')); ?></p>
      </div>
      <br />
      <?php endif; ?>
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Pengaturan</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <?php $__currentLoopData = $siswa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <section class="content">
      <div class="row">
        <div style="margin-left: 15%" class="col-8">
          <div class="card">
            <form class="" action="<?php echo e(route('pengaturan.update', $key->id)); ?>" method="post" enctype="multipart/form-data">
              <?php echo csrf_field(); ?>
              <input type="hidden" name="_method" value="PATCH">
              <input type="hidden" value="<?php echo e($key->user->foto); ?>" name="foto_lama" >
            <div class="card-header">
              <h4>Data Pribadi</h4>
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <center><img src="storage/app/public/upload_foto_siswa/<?php echo e(auth::user()->foto); ?>" width="150px" class="img-circle elevation-2"></center>
                    </div>
                    <div class="form-group">
                      <label for="">Ganti Foto</label>
                      <input type="file" name="foto" value="<?php echo e($key->foto); ?>" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="">Jenis Kelamin</label>
                      <div class="row">
                        <div class="col-6">
                          <input <?php if($key->user->jk == 1): ?> checked
                          <?php endif; ?> type="radio" name="jk" value="1"> Laki-laki
                        </div>
                        <div class="col-6">
                          <input <?php if($key->user->jk == 2): ?> checked
                          <?php endif; ?> type="radio" name="jk" value="2"> Perempuan
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="">Agama</label>
                      <select class="form-control" name="id_agama">
                        <option value="<?php echo e($key->user->id_agama); ?>"><?php echo e($key->user->agama->agama); ?></option>
                        <?php $__currentLoopData = $agama; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($agm->id); ?>"><?php echo e($agm->agama); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-8">
                    <div class="form-group">
                      <label for="nama">Nomer Induk Siswa (NIS)</label>
                      <input type="text" name="nis" value="<?php echo e($key->nis); ?>" required class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama Lengkap</label>
                      <input type="text" name="nama" value="<?php echo e($key->user->nama); ?>" required class="form-control">
                    </div>
                    <div class="row">
                      <div class="col-6">
                        <label for="nama">Tempat Lahir</label>
                        <input type="text" name="tempatlahir" value="<?php echo e($key->user->tempatlahir); ?>" required class="form-control">
                      </div>
                      <div class="col-6">
                        <label for="nama">Tanggal Lahir</label>
                        <input type="date" name="tanggallahir" value="<?php echo e($key->user->tanggallahir); ?>" required class="form-control">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="nama">Nomer Telepon</label>
                      <input type="number" name="nomertelepon" value="<?php echo e($key->user->telepon); ?>" required class="form-control">
                    </div>
                    
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="">Alamat</label>
                      <textarea name="alamat" class="form-control" rows="5"><?php echo e($key->user->alamat); ?></textarea>
                    </div>
                  </div>
                </div>
                <hr><h4>Pengaturan Akses</h4>
                <div class="row">

                  <div class="col-6">
                    <div class="form-group">
                      <label for="">Email</label>
                      <input type="text" name="email" value="<?php echo e($key->user->email); ?>" required class="form-control">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="">Password</label>
                      <input type="password" name="password" value="<?php echo e($key->user->password); ?>" class="form-control">
                      
                    </div>
                  </div>
                </div>
                <hr><h4>Orang Tua</h4>
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <label for="">Nama Ayah</label>
                      <input type="text" name="namaayah" value="<?php echo e($key->nama_ayah); ?>" class="form-control" required>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="">Nama Ibu</label>
                      <input type="text" name="namaibu" value="<?php echo e($key->nama_ibu); ?>" class="form-control" required>
                    </div>
                  </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="clearfix">

            </div>
            <div class="card-body">
              <center><button type="submit" name="submit" class="btn btn-success btn-lg" style="width : 50%">Simpan</button> </center
            </div>
          </form>
            <!-- /.card-body -->

          </div>
          <!-- /.card -->
        </div>
      </div><!-- /.row -->
    </section>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <!-- Main content -->

  <!-- /.content -->
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>