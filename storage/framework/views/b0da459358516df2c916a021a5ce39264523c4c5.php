<?php $__env->startSection('title', 'Quiz'); ?>

<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <?php if(\Session::has('alert')): ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p><?php echo e(\Session::get('alert')); ?></p>
      </div>
      <br />
      <?php endif; ?>
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?php echo e($quiz->judul); ?></h1>
        </div>

      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <?php if(checkPermission(['siswa'])): ?>
        <div class="col-md-4">
          <div class="card">
            <div class="card-header">
              <?php $__currentLoopData = $hasil->where('id_users','=',Auth::user()->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nilai): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <h3 class="card-title">Nilai : <?php echo e($nilai->nilai); ?>  </h3>
                  <h3 class="card-title">Benar : <?php echo e($nilai->benar); ?>  </h3>
                  <h3 class="card-title">Salah : <?php echo e($nilai->salah); ?>  </h3>
                  <h3 class="card-title">Kosong : <?php echo e($nilai->kosong); ?>  </h3>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>
            <div class="clearfix"></div>
            <div class="card-body table-responsive p-3">
              <h5>Info :</h5>
              <p><?php echo e($quiz->info); ?></p>
            </div>
            <a href="mulai/<?php echo e($quiz->id); ?>" class="btn btn-success form-control">Mulai</a>




          </div>
        </div>
      <?php endif; ?>
      <?php if(checkPermission(['pengajar'])): ?>
        
      <?php endif; ?>
      <div  class="col-md-8">

        <div class="card">
          <div class="card-header">
            <h3 class="card-title"><?php echo e(Auth::user()->nama); ?>  </h3>
          </div>
          <div class="clearfix"></div>
          <div class="card-body table-responsive p-3">
            <form class="" action="<?php echo e(route('komen.store')); ?>" method="post" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
              <input type="hidden" class="form-control" name="idquiz" value="<?php echo e($quiz->id); ?>">
              <div class="form-group">
                <input type="text" class="form-control" name="pesan" value="">
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-info form-control" name="submit">Kirim</button>
              </div>
            </form>
          </div>
        </div>
        <h2>Komentar</h2>
        <?php $__currentLoopData = $quiz->komen()->latest()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $komen): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><?php echo e($komen->users->nama); ?>  <span style="float: right">| <?php echo e($quiz->created_at->diffForHumans()); ?> |</span></h3>
            </div>
            <div class="clearfix"></div>
            <div class="card-body table-responsive p-3">
              <p><?php echo e($komen->pesan); ?></p>
            </div>
          </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>




      </div>





    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>