<?php $__env->startSection('title', 'Halaman Dashboard'); ?>

<?php $__env->startSection('content'); ?>
<div class="content-wrapper" style="background-image: url('dist/img/bg_out.jpg'">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Berita Terkini</h1>
        </div><!-- /.col -->
        <!-- <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v2</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content" >
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <?php $__currentLoopData = $berita; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="col-6">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?php echo e($key->judul); ?></h3>


              </div>
              <div class="clearfix"></div>
              <div class="card-body table-responsive">
                <div class="row">
                  <div class="col-md-8">
                    <h5><?php echo e($key->users->nama); ?></h5>
                  </div>
                  <div class="col-md-4">
                    <p style="text-align : right"><?php echo e($key->created_at->diffForHumans()); ?></p>
                  </div>
                </div>



              </div>
              <a href="home/<?php echo e($key->id); ?>" class="btn  form-control" style="color : #fff; background-color : #46a8af">Lanjut membaca ...</a>
            </div>
          </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

      <!-- Main row -->
      <div class="row">
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>