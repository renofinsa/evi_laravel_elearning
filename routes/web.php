<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// // Route::resource('/quiz/soal', 'QuizSoalController');
// Route::post('/quiz/{soal}', 'QuizSoalController@store')->name('quiz.soal.store');
// Route::delete('/quiz/{soal}', 'QuizSoalController@destroy')->name('soal.soal.destroy');

// Route::resource('/quiz/{quiz}/soal', 'QuizSoalController');
//
//
//





Route::group(['middleware'=>'auth'], function(){
  Route::resource('/kelas', 'KelasController');
  Route::resource('/siswa', 'SiswaController');
  Route::resource('/admin', 'AdminController');
  Route::resource('/pengajar', 'PengajarController');
  Route::resource('/matapelajaran', 'MatpelController');
  Route::resource('/matpelpengajar', 'MatpelpengajarController');
  Route::resource('/berita', 'BeritaController');
  Route::resource('/home', 'InfoController');
  Route::resource('/materi', 'MateriController');
  Route::resource('/quiz', 'QuizController');
  Route::resource('/quizsiswa/mulai/jawaban', 'JawabanController');
  Route::resource('/komen', 'KomenController');
  // Route::delete('/quiz/{soal}', 'QuizController@destroysoal');
  Route::resource('/quizz', 'MulaiQuizController');
  Route::resource('/soal', 'SoalController');
  Route::resource('/quizsiswa/mulai', 'SoalController');
  Route::resource('/quizsiswa', 'JawabanTerpilihController');
  Route::resource('/semester', 'SemesterController');
  Route::resource('/pengaturan', 'ProfileController');
  Route::resource('/hasil', 'HasilController');
  Route::get('/get_hasil/{id}', 'MakePdfController@edit');


  Route::get('/dashboard_admin', ['middleware'=>'check-permission:admin', 'uses'=>'HomeController@allAdmin']);
  Route::get('/dashboard_pengajar', ['middleware'=>'check-permission:pengajar', 'uses'=>'HomeController@allPengajar']);
  Route::get('/dashboard_siswa', ['middleware'=>'check-permission:siswa', 'uses'=>'HomeController@allSiswa']);

});
